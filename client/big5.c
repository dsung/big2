#include "big2.h"

int
is_break(c)
int c;
{
if (c==CTRLC || c==CTRLD) return(1);
return(0);
}

int
is_back(c)
int c;
{
if (c==BACK || c==CTRLH) return(1);
return(0);
}


int 
is_d_a(c)
int c;
{
if ((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97 && c<=122)) return(1);
return(0);
}

int
is_a(c)
int c;
{
if ((c>=65 && c<=90) || (c>=97 && c<=122)) return(1);
return(0);
}

int 
is_high_bit_big5(c)
int c;
{
if 
((c >= 0xa1 && c <=0xfe) || (c >= 0x8e && c<=0xa0) || (c >= 0x81 && c <= 0x8d)) 
return YEA;
return NA;
}

int
is_low_bit_big5(c)
int c;
{
if
((c >=0x40 && c<= 0x7e) || (c >=0xa1 && c <=0xfe))
return YEA;
return NA;
}
 
int
is_big5(c,c2)
int c,c2;
{
if (is_high_bit_big5(c) &&  is_low_bit_big5(c2)) return(1);
return(0);
}

