#include "big2.h"

int opener;
time_t now;

read_info()
{
switch(readch()) {
case 'A':
switch(readch()) {
case 'e':
self.level=0;
xwaddstr(win1,"\n*** 你現在變回一般玩家了.",0);
break;

case 'm':
get_srvmsg(tmp,'\n');
switch(readch()) {
case 'G':
sprintf(buffer,"\n*** %s 正在玩遊戲, 不能變更他的錢數!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'N':
sprintf(buffer,"\n*** 系統裡沒有 %s 這位玩家!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'Y':
get_srvmsg(buffer,'#');
sprintf(buffer2,"\n*** 你將 %s 的錢數更改為 $%s.",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;
default:
bad_srvinfo();
}
break;

case 'n':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** 系統最大上線人數變更為: %s",tmp);
xwaddstr(win1,buffer,0);
break;

case 'k':
get_srvmsg(tmp,'\n');
switch(readch()) {
case 'Y':
sprintf(buffer,"\n*** %s 已被砍去!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'N':
sprintf(buffer,"\n*** %s 不在線上!",tmp);
xwaddstr(win1,buffer,0);
break;
default:
bad_srvinfo();
}
break;

case 'l':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** DELAY SECOND is now: %s",tmp);
xwaddstr(win1,buffer,0);
break;

case 'M':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** MAX error times is now: %s",tmp);
xwaddstr(win1,buffer,0);
break;

case 'p':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n*** 伺服器公告事項第 %s 項已改為: %s",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;

case 'r':
if (readch()=='Y') 
xwaddstr(win1,"\n*** 伺服器 read_log 切換為 ON",0);
else
xwaddstr(win1,"\n*** 伺服器 read_log 切換為 OFF",0);
break;

case 'w':
if (readch()=='Y') 
xwaddstr(win1,"\n*** 伺服器 write_log 切換為 ON",0);
else
xwaddstr(win1,"\n*** 伺服器 write_log 切換為 OFF",0);
break;

default:
bad_srvinfo();
}
break;

case 'B': /* BASIC SERVER INFO */
switch(readch()) {
case 'a':
switch(readch()) {
case 'Y':
self.level=100;
xwaddstr(win1,"\n*** 密碼正確!! 你現在是一位伺服器系統管理者了喔!",0);
break;
case 'N':
xwaddstr(win1,"\n*** 密碼錯誤!! 現在你的好奇心應該滿足了吧? :D",0);
break;
default:
bad_srvinfo();
}
break;

case 'b':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n**<<動作>>** %s %s",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;

case 'c': /* admin sent all */
get_srvmsg(buffer,'\n');
if (beeper) beep();
sprintf(buffer2,"\n*** 作者說: %s",buffer);
xwaddstr(win1,buffer2,0);
break;

case 'd': /* admin changes money */
get_srvmsg(tmp,'#');
self.money=atoi(tmp);
sprintf(buffer,"\n*** 系統管理者將你的錢數變更為: $%d.",self.money);
xwaddstr(win1,buffer,0);
break;

case 'e':
get_srvmsg(tmp,'\n');
if (!strcmp(tmp,"-1")) 
sprintf(buffer,"\n*** 大老二伺服器停機囉!");
else
sprintf(buffer,"\n*** 抱歉!! 系統將 %s 秒後自行毀滅....請馬上離開",tmp);
xwaddstr(win1,buffer,0);
break;

case 'f':
get_srvmsg(tmp,'\n');
switch(readch()) {
case 'A': /* be asked */
if (self.away_flag) 
sprintf(buffer,"Be%s\n%s\n%s\n%s\n%02d#%s\n%s\n%s\n%d#%d#%d#%d#%d#1%s\n",tmp,username,domainname,ipaddress,self.desk,on_game_check(self.on_game),self.kernel,logintime,idlemin,self.money,self.win,self.lose,self.login,self.away_note);
else
sprintf(buffer,"Be%s\n%s\n%s\n%s\n%02d#%s\n%s\n%s\n%d#%d#%d#%d#%d#0",tmp,username,domainname,ipaddress,self.desk,on_game_check(self.on_game),self.kernel,logintime,idlemin,self.money,self.win,self.lose,self.login);
new_write(sockfd,buffer,strlen(buffer));
break;
case 'N':
sprintf(buffer,"\n*** 系統裡沒有 %s 這位玩家!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'O':
get_srvmsg(finger.uname,'\n');
get_srvmsg(finger.domain,'\n');
get_srvmsg(finger.ip,'\n');
get_srvmsg(tmp2,'#'); finger.money=atoi(tmp2);
get_srvmsg(tmp2,'#'); finger.win=atoi(tmp2);
get_srvmsg(tmp2,'#'); finger.lose=atoi(tmp2);
get_srvmsg(tmp2,'#'); finger.login=atoi(tmp2);
sprintf(buffer,"\n*** %s 是 %s@%s (%s).",tmp,finger.uname,finger.domain,finger.ip);
xwaddstr(win1,buffer,1);
if (self.on_game)
sprintf(buffer2,"\n*** 金額: $%d  贏/輸次數: %d/%d\n*** 上站次數: %d\n*** 目前不在線上.",finger.money,finger.win,finger.lose,finger.login);
else
sprintf(buffer2,"\n*** 金額: $%d  贏/輸次數: %d/%d  上站次數: %d\n*** 目前不在線上.",finger.money,finger.win,finger.lose,finger.login);
xwaddstr(win1,buffer2,0);
break;
case 'Y':
get_srvmsg(buffer,'\n');
get_srvmsg(tmp2,'\n');
get_srvmsg(tmp3,'\n');
sprintf(buffer2,"\n*** %s 是 %s@%s (%s).",tmp,buffer,tmp2,tmp3);
xwaddstr(win1,buffer2,1);
get_srvmsg(tmp,'#');
get_srvmsg(tmp2,'\n');
get_srvmsg(buffer,'\n');
if (self.on_game) 
sprintf(buffer2,"\n*** 桌號:(%s)  遊戲中:%s\n*** 作業系統:<%s>.",tmp,tmp2,buffer);
else
sprintf(buffer2,"\n*** 桌號:(%s)  遊戲中:%s  作業系統:<%s>.",tmp,tmp2,buffer);
xwaddstr(win1,buffer2,1);
get_srvmsg(tmp5,'#');
get_srvmsg(tmp4,'\n');
get_srvmsg(buffer,'#');
get_srvmsg(tmp2,'#');
get_srvmsg(tmp3,'#');
get_srvmsg(tmp,'#');
finger.away_flag=0;
if (readch()=='1') {get_srvmsg(finger.away_note,'\n'); finger.away_flag=1;}
if (self.on_game)
sprintf(buffer2,"\n*** 金額: $%s  贏/輸次數: %s/%s\n*** 上站次數: %s\n*** 閒置: %s 分鐘\n*** 上線: <%s>",buffer,tmp2,tmp3,tmp,tmp5,tmp4);
else
sprintf(buffer2,"\n*** 金額: $%s  贏/輸次數: %s/%s  上站次數: %s\n*** 閒置: %s 分鐘  上站時刻: <%s>",buffer,tmp2,tmp3,tmp,tmp5,tmp4);
xwaddstr(win1,buffer2,1);
if (finger.away_flag) {
sprintf(buffer2,"\n*** 他目前臨時不在. 留言: %s",finger.away_note);
xwaddstr(win1,buffer2,1);
}
wrefresh(win1);
break;
default:
bad_srvinfo();
}
break;

case 'g':
sprintf(tmp,"Bg");
new_write(sockfd,tmp,strlen(tmp));
break;

case 'h':
get_srvmsg(finger.nick,'\n');
get_srvmsg(finger.away_note,'\n');
sprintf(buffer,"\n*** %s 目前臨時不在. 留言: %s",finger.nick,finger.away_note);
xwaddstr(win1,buffer,0);
break;

case 'i':
switch(readch()) {
case 'I':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'#');
if (beeper) beep();
time(&now);
if (self.away_flag)
{char *a;
a=ctime(&now);
a[strlen(a)-1]='\0';
sprintf(buffer2,"\n*** %s 誠摯邀請你加入第%s桌. <%s>",tmp,buffer,a);
sprintf(buffer,"Bd%s\n%s\n",tmp,self.away_note);
new_write(sockfd,buffer,strlen(buffer));
}
else
sprintf(buffer2,"\n*** %s 誠摯邀請你加入第%s桌.",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;
case 'Y':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** 邀請 %s 加入你這桌.",tmp);
xwaddstr(win1,buffer,0);
break;
case 'N':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 並不在線上!",tmp);
xwaddstr(win1,buffer,0);
break;
default:
bad_srvinfo();
}
break;

case 'j': /* others join the current desk */
get_srvmsg(tmp,'#');
get_srvmsg(tmp2,'\n');
get_srvmsg(tmp3,'\n');
sprintf(buffer,"\n*** %s (%s@%s) 加入了本桌.",tmp,tmp2,tmp3);
xwaddstr(win1,buffer,0);
deskman++;
break;

case 'k':
if (idlemin >= 3) {
xwaddstr(win1,"\n*** 你於遊戲中閒置過久, 已被對手砍去.",0);
self.on_game=0;
client_exit(0);
}
break;

case 'l':
desk_list(1);
break;

case 'm':
switch(readch()) {
case 'N':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 並不在線上!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'M':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
time(&now);
if (self.away_flag)
{char *a;
a=ctime(&now);
a[strlen(a)-1]='\0';
sprintf(buffer2,"\n*%s* %s <%s>",tmp,buffer,a);
if (beeper) beep();
sprintf(buffer,"Bd%s\n%s\n",tmp,self.away_note);
new_write(sockfd,buffer,strlen(buffer));
}
else
sprintf(buffer2,"\n*%s* %s",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;
case 'Y':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n-> *%s* %s",tmp,buffer);
xwaddstr(win1,buffer2,0);
wrefresh(win1);
break;
default:
bad_srvinfo();
}
break;

case 'n':
xwaddstr(win1,"\n*** 你的密碼已經更改成功\了!",0);
break;

case 'o':
get_srvmsg(tmp,'\n');
switch(readch()) {
case 'A':
get_srvmsg(tmp2,'\n');
sprintf(buffer,"\n*** 桌長 %s 將 %s 等級提升為本桌桌長.",tmp,tmp2);
xwaddstr(win1,buffer,0);
break;
case 'D':
sprintf(buffer,"\n*** %s 不屬於本桌!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'N':
sprintf(buffer,"\n*** %s 不在線上!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'O':
sprintf(buffer,"\n*** 你將本桌的 %s 等級提升為桌長.",tmp);
xwaddstr(win1,buffer,0);
break;
case 'o':
sprintf(buffer,"\n*** 桌長 %s 將你的等級提升為本桌桌長.",tmp);
xwaddstr(win1,buffer,0);
self.level=30;
break;
default:
bad_srvinfo();
}
break;

case 'p':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 已離開本桌.",tmp);
xwaddstr(win1,buffer,0);
deskman--;
break;

case 'q':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 已離開網路休閒大老二.",tmp);
xwaddstr(win1,buffer,0);
deskman--;
break;

case 'r':
if (readch()=='N') {
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** 所給定要排名的名次需介於 1 到 %s 之間",tmp);
xwaddstr(win1,buffer,0);
return;}
rank_list(1,0);
break;

case 's':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n<%s> %s",tmp,buffer);
xwaddstr(win1,buffer2,0);
break;

case 't':
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n*** 桌長 %s 將本桌標題設為: %s",tmp,buffer); 
xwaddstr(win1,buffer2,0);
break;

case 'u':
user_list(1);
break;

case 'v':
get_srvmsg(tmp,'\n');
get_srvmsg(tmp2,'#');
sprintf(buffer2,"\n*** 本次遊戲賭金倍數為: %s",tmp2);
xwaddstr(win1,buffer2,0);
bet_times=atoi(tmp2);
break;

case 'w':
who_list(0,1);
break;

case 'x':
xwaddstr(win1,"\n*** 你這桌已經有人在玩囉! 現在是不準改賭金倍數的!",0);
break;

case 'T': /* Time PERIOD */
get_srvmsg(tmp,'@');
total_online=atoi(tmp);
break;

default:
bad_srvinfo();
}
break;

case 'G':
switch(readch()) {
case 'i': /* game initial situation */
switch(readch()) {
case 'A':
xwaddstr(win1,"\n*** 本桌已經有其他人在玩大老二了!",0);
break;
case 'D':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 不屬於本桌!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'G': /* be invited */
get_srvmsg(tmp,'#'); threecard=atoi(tmp);
get_srvmsg(o[0].nick,'\n');
get_srvmsg(o[1].nick,'\n');
get_srvmsg(o[2].nick,'\n');
get_srvmsg(tmp,'#'); self.on_game=atoi(tmp);
get_srvmsg(tmp,'#'); o[0].money=atoi(tmp);
get_srvmsg(tmp,'#'); o[1].money=atoi(tmp);
get_srvmsg(tmp,'#'); o[2].money=atoi(tmp);
if (threecard==1) strcpy(tmp,"(有玩三條的)"); else strcpy(tmp,"(沒玩三條的)");
if (self.on_game==2) 
sprintf(lastmsg,"*** 遊戲開始! 對手依序為: %s, %s, 和 %s (%s 開盤) %s.",o[0].nick,o[1].nick,o[2].nick,o[2].nick,tmp);
if (self.on_game==3) 
sprintf(lastmsg,"*** 遊戲開始! 對手依序為: %s, %s, 和 %s (%s 開盤) %s.",o[0].nick,o[1].nick,o[2].nick,o[1].nick,tmp);
if (self.on_game==4) 
sprintf(lastmsg,"*** 遊戲開始! 對手依序為: %s, %s, 和 %s (%s 開盤) %s.",o[0].nick,o[1].nick,o[2].nick,o[0].nick,tmp);
opener=0;
pause_position=0;
break;
case 'H': /* invite others */
get_srvmsg(tmp,'#'); threecard=atoi(tmp);
get_srvmsg(o[0].nick,'\n');
get_srvmsg(o[1].nick,'\n');
get_srvmsg(o[2].nick,'\n');
self.on_game=1;
get_srvmsg(tmp,'#'); o[0].money=atoi(tmp);
get_srvmsg(tmp,'#'); o[1].money=atoi(tmp);
get_srvmsg(tmp,'#'); o[2].money=atoi(tmp);
if (threecard==1) strcpy(tmp,"(有玩三條的)"); else strcpy(tmp,"(沒玩三條的)");
sprintf(lastmsg,"*** 遊戲開始! 對手依序為: %s, %s, 和 %s (由你開盤) %s.",o[0].nick,o[1].nick,o[2].nick,tmp);
opener=1;
pause_position=0;
break;
case 'J':
get_srvmsg(g[0].nick,'\n');
get_srvmsg(g[1].nick,'\n');
get_srvmsg(g[2].nick,'\n');
get_srvmsg(g[3].nick,'\n');
get_srvmsg(tmp,'#'); g[0].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[1].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[2].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[3].money=atoi(tmp);
sprintf(lastmsg,"*** 本桌已有 4 人在玩遊戲. 他們是 %s, %s, %s 和 %s.",g[0].nick,g[1].nick,g[2].nick,g[3].nick);
break;
case 'M': /* people starts to play here */
get_srvmsg(g[0].nick,'\n');
get_srvmsg(g[1].nick,'\n');
get_srvmsg(g[2].nick,'\n');
get_srvmsg(g[3].nick,'\n');
get_srvmsg(tmp,'#'); g[0].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[1].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[2].money=atoi(tmp);
get_srvmsg(tmp,'#'); g[3].money=atoi(tmp);
sprintf(lastmsg,"*** 本桌有 4 人開始遊戲. 他們是 %s, %s, %s 和 %s.",g[0].nick,g[1].nick,g[2].nick,g[3].nick);
break;
case 'N':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 不在線上!",tmp);
xwaddstr(win1,buffer,0);
break;
case 'O':
get_srvmsg(tmp,'\n');
sprintf(buffer,"\n*** %s 已經在玩了!",tmp);
xwaddstr(win1,buffer,0);
break;
default:
bad_srvinfo();
}
break;

case 'g': /* server initial give */
nn=get_srvcards(mycard);
self.cardleft=nn;
o[0].cardleft=nn;
o[1].cardleft=nn;
o[2].cardleft=nn;
switch(readch()) {
case '1':
self.on_game=1;
first_hand=1;
break;
case '2':
self.on_game=2;
first_hand=0;
break;
case '3':
self.on_game=3;
first_hand=0;
break;
case '4':
self.on_game=4;
first_hand=0;
break;
default:
bad_srvinfo();
}
sort_cards(self.cardleft);
showcard_screen_init(0);
show_cards(0,1);
last_n=0;
last_n_fake=0;
pass_time=0;
tabkey_mode=0;
cardposx=0;
card_go_out = 0;
bzero((char *)queue,sizeof(queue));
qlen=0;
if (opener) {
int i;
if (self.money>=10000000 && o[0].money>=10000000 && o[1].money>=10000000 && o[2].money>=10000000) {i=(int)(self.money/100); if (i>1000000) i=1000000;} 
else
if (self.money>=1000000 && o[0].money>=1000000 && o[1].money>=1000000 && o[2].money>=1000000) {i=(int)(self.money/100); if (i>100000) i=100000;} 
else
if (self.money>=100000 && o[0].money>=100000 && o[1].money>=100000 && o[2].money>=100000) {i=(int)(self.money/100); if (i>10000) i=10000;} 
else {
if (o[0].money == o[1].money && o[1].money == o[2].money) i=(int)(o[0].money/100);
else i=(int)(self.money/100);
if (i>1000) i=1000; if (i<1) i=1;
}
sprintf(tmp,"Gb%d\n",i);
new_write(sockfd,tmp,strlen(tmp));
}
break;

case 'h': /* others followingly give */
last_n_fake=get_srvcards(last);
see_card(0,last_n_fake);
if (self.on_game==2) o[2].cardleft-=last_n_fake;
if (self.on_game==3) o[1].cardleft-=last_n_fake;
if (self.on_game==4) o[0].cardleft-=last_n_fake;
self.on_game-=1;
show_cards(0,0);
if (tabkey_mode) {xmvwaddstr(winB,4,0,"→"); mvwaddch(winB,4,2+(cardposx*4),'*'); touchwin(win1); wrefresh(winB); wrefresh(win0);}
check_win_or_lose(0);
break;

case 'j': /* self give */
last_n_fake=get_srvcards(last);
see_card(1,last_n_fake);
self.cardleft-=last_n_fake;
self.on_game=4;
show_cards(0,1);
cardposx=0;
if (tabkey_mode) {xmvwaddstr(winB,4,0,"→"); mvwaddch(winB,4,2+(cardposx*4),'*'); touchwin(win1); wrefresh(winB); wrefresh(win0);}
bzero((char *)queue,sizeof(queue));
qlen=0;
check_win_or_lose(0);
break;

case 'r': /* others init give */
switch(readch()) {
case '0':
arrow1=0;
break;
case '1':
arrow1=1;
break;
case '2':
arrow1=2;
break;
case '3':
arrow1=3;
break;
default:
bad_srvinfo();
}
g[0].cardleft=13;
g[1].cardleft=13;
g[2].cardleft=13;
g[3].cardleft=13;
last_n=0;
last_n_fake=0;
showcard_screen_init(1);
show_cards(1,0);
break;

case 's': /* others init give too (join followingly case) */
get_srvmsg(tmp,'#'); g[0].cardleft=atoi(tmp);
get_srvmsg(tmp,'#'); g[1].cardleft=atoi(tmp);
get_srvmsg(tmp,'#'); g[2].cardleft=atoi(tmp);
get_srvmsg(tmp,'#'); g[3].cardleft=atoi(tmp);
get_srvmsg(tmp,'\n');
if (!strcmp(tmp,g[0].nick)) arrow1=0;
if (!strcmp(tmp,g[1].nick)) arrow1=1;
if (!strcmp(tmp,g[2].nick)) arrow1=2;
if (!strcmp(tmp,g[3].nick)) arrow1=3;
last_n=0;
last_n_fake=0;
showcard_screen_init(1);
show_cards(1,0);
break;

case 'o':
sprintf(lastmsg,"*** 遊戲結束.");
pause_position=0;
if (self.on_game) {
self.on_game=0;
self.cardleft=0;
showcard_screen_end(0); } 
else
showcard_screen_end(1);
break;

case 'y': /* the playing four following give */
last_n_fake=get_srvcards(last);
see_card(2,last_n_fake);
g[arrow1].cardleft -= last_n_fake;
arrow1++;
if (arrow1==4) arrow1=0;
show_cards(1,0);
check_win_or_lose(1);
break;

default:
bad_srvinfo();
}
break;

case 'Q':
xwaddstr(win1,"\n*** 大老二伺服器強迫將你終結!",0);
client_exit(2);
break;

case 'S': /* SERVER INFO ABOUT SELF */
switch(readch()) {
case 'b':
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n*** 動作: %s %s",self.nick,buffer);
xwaddstr(win1,buffer2,0);
break;

case 'j': 
get_srvmsg(tmp,'#');
self.desk=atoi(tmp);
get_srvmsg(buffer,'#');
get_srvmsg(tmp2,'#');
bet_times=atoi(tmp2);
deskman=atoi(buffer);
get_srvmsg(buffer3,'\n'); /* topic */
get_srvmsg(buffer2,'\n'); /* users */
sprintf(buffer,"\n*** 你加入了第%02d桌.\n*** 本桌標題: %s\n*** 本桌賭金倍數為: %s\n*** 本桌玩家有: %s",self.desk,buffer3,tmp2,buffer2);
if (deskman==1) self.level=30;
xwaddstr(win1,buffer,0);
break;

case 'p':
if (readch()=='A') {sprintf(lastmsg,"*** 你中途離開囉!"); showcard_screen_end(1);}
sprintf(buffer,"\n*** 你離開了第%02d桌.",self.desk);
xwaddstr(win1,buffer,0);
self.desk= -1;
self.level=0;
self.on_game=0;
self.cardleft=0;
deskman= -1;
break;

case 's':
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n> %s",buffer);
xwaddstr(win1,buffer2,0);
break;

case 't':
get_srvmsg(buffer,'\n');
sprintf(buffer2,"\n*** 你將本桌標題設定為: %s",buffer);
xwaddstr(win1,buffer2,0);
break;

case 'v':
get_srvmsg(tmp,'#');
sprintf(buffer,"\n*** 本次遊戲賭金倍數為: %s",tmp);
xwaddstr(win1,buffer,0);
bet_times=atoi(tmp);
break;

default:
bad_srvinfo();
}
break; /* Self */

default:
bad_srvinfo();
}
condition_win();
}

bad_srvinfo()
{
xwaddstr(win1,"\n*** 警告!! 收到無法辨認的伺服器訊息!!!",0);
badsrvinfo++;
if (badsrvinfo>3) {
xwaddstr(win1,"\n*** 哇....錯誤次數太多, Sorry...強制退出.",0);
error_exit();
}
}

