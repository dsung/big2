#include "big2.h"

big2rule(a,argc)
int *a,argc;
{
if (argc==1) return(rule1(a));
if (argc==2) return(rule2(a));
if (argc==3 && threecard) return(rule3(a)); else 
if (argc==3) return(2);
if (is_fullhouse(mycard,a)) return(rule4(a));
if (is_smith(mycard,a)) return(rule5(a));
if (is_serial(mycard,a)) return(rule6(a));
return(1);
}

rule1(a) /* for single */ /* ok */
int *a;
{
if (!last_n) return(0);
if (last_n!=1) return(1);
if (convert3(mycard[a[0]-1].point)>convert3(last[0].point)) return(1); 
if (mycard[a[0]-1].point==last[0].point)
if (mycard[a[0]-1].suit<last[0].suit) return(1);
return(0);
}

rule2(a) /* for pairs */ /* ok */
int *a;
{
if (mycard[a[0]-1].point!=mycard[a[1]-1].point) return(1);
if (!last_n) return(0);
if (last_n!=2) return(1);
if (convert3(mycard[a[0]-1].point)>convert3(last[0].point)) return(1);
if (mycard[a[0]-1].point==last[0].point) 
if (mycard[a[0]-1].suit<last[0].suit) return(1);
return(0);
}

rule3(a) /* for Tripe-cards */
int *a;
{
if (mycard[a[0]-1].point!=mycard[a[1]-1].point || mycard[a[0]-1].point!=mycard[a[2]-1].point || mycard[a[1]-1].point!=mycard[a[2]-1].point) return(1);
if (!last_n) return(0);
if (last_n!=3) return(1);
if (convert3(mycard[a[0]-1].point)>convert3(last[0].point)) return(1);
return(0);
}

rule4(a) /* for Full-House */
int *a;
{
int j[5];
j[0]=1; j[1]=2; j[2]=3; j[3]=4; j[4]=5; /* for last-1 */
if (!last_n) return(0);
if (last_n!=5) return(1);
if (!is_fullhouse(last,j)) return(1);
if (convert3(mycard[a[2]-1].point)>convert3(last[2].point)) return(1);
return(0);
}

rule5(a) /* for given 'smith' (iron-stick) */
int *a;
{
int j[5];
j[0]=1; j[1]=2; j[2]=3; j[3]=4; j[4]=5; /* for last-1 */
if (last_n!=5) return(0);
if (is_fullhouse(last,j)) return(0);
if (is_serial_same_pattern(last,j)) return(1);
if (is_serial(last,j)) return(0);
if (convert3(mycard[a[2]-1].point)>convert3(last[2].point)) return(1);
return(0);
}

rule6(a) /* for given 'serial' (sequences) */
int *a;
{
int j[5];
j[0]=1; j[1]=2; j[2]=3; j[3]=4; j[4]=5; /* for last-1 */
if (!last_n) return(0);
if (!is_serial_same_pattern(mycard,a)) {
if (last_n!=5) return(1); 
if (is_fullhouse(last,j) || is_smith(last,j) || is_serial_same_pattern(last,j)) return(1);
return(rule7(a));
}  
if (!is_serial_same_pattern(last,j)) return(0);
return(rule7(a));
}

rule7(a) /* compare two 'serial' cards */
int *a;
{
int d;
char b,c;
b=convert3(mycard[a[0]-1].point);
c=convert3(last[0].point);
if (b!='A' && c!='A') {
if (b>c) return(1);
if (b<c) return(0);
if (mycard[a[0]-1].suit<last[0].suit) return(1);
return(0);
}
if (b=='A' && c=='A') {
b=convert3(mycard[a[1]-1].point);
c=convert3(last[1].point);
if (b>c) return(0);
if (b<c) return(1);
if (last[1].point==1) d=2; else d=0;
if (mycard[a[d]-1].suit>last[d].suit) return(0);
return(1);
}
if (b=='A') {if (mycard[a[1]-1].point==1) return(1); else return(0);}
if (c=='A') {if (last[1].point==1) return(0); else return(1);}
}

is_fullhouse(b,a)
int *a;
struct card *b;
{
if (b[a[0]-1].point!=b[a[1]-1].point || b[a[3]-1].point!=b[a[4]-1].point) return(0);
if (b[a[1]-1].point==b[a[2]-1].point || b[a[3]-1].point==b[a[2]-1].point) return(1);
return(0);
}

is_smith(b,a)
int *a;
struct card *b;
{
if (b[a[1]-1].point!=b[a[2]-1].point || b[a[2]-1].point!=b[a[3]-1].point) return(0);
if (b[a[4]-1].point==b[a[3]-1].point || b[a[0]-1].point==b[a[1]-1].point) return(1);
return(0);
}

is_serial(b,a)
int *a;
struct card *b;
{
int n;
char c[5];
for(n=0;n<5;n++) c[n]=convert3(b[a[n]-1].point);
if (c[0]=='A' && c[1]=='J' && c[2]=='K' && c[3]=='L' && c[4]=='M') return(1);
if (c[0]=='A' && c[1]=='B' && c[2]=='K' && c[3]=='L' && c[4]=='M') return(1);
if (c[0]!='A' && c[0]==c[1]-1 && c[1]==c[2]-1 && c[2]==c[3]-1 && c[3]==c[4]-1) return(1);
return(0);
}

is_serial_same_pattern(b,a)
int *a;
struct card *b;
{
if (is_serial(b,a) && b[a[0]-1].suit==b[a[1]-1].suit && b[a[1]-1].suit==b[a[2]-1].suit && b[a[2]-1].suit==b[a[3]-1].suit && b[a[3]-1].suit==b[a[4]-1].suit) return(1);
return(0);
}

