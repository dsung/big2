#include "big2.h"

char tmp[1024], buffer[3000];
char *username, myhostname[80], *ipaddress, *domainname;
struct in_addr myhostaddr;
struct hostent *host;
struct utsname kernel;
struct player self;
int color_mode;
int term_type;

WINDOW *win0, *win1, *winc, *winu, *win2;
WINDOW *winA, *winB, *winS, *wins, *winC;

void get_local_info()
{
  int i;
  username = getpwuid(getuid())->pw_name;
  gethostname(myhostname, sizeof(myhostname));
  host = gethostbyname(myhostname);
  if (host)
  {
    bcopy(host->h_addr, (char *) &myhostaddr, sizeof(myhostaddr));
    ipaddress = inet_ntoa(myhostaddr);
    domainname = (char *) host->h_name;
  }
  if (strchr(domainname, '.') == NULL)
  {
    i = strlen(myhostname);
    myhostname[i] = '.';
    myhostname[i + 1] = (char) NULL;
    host = gethostbyname(myhostname);
    if (host)
    {
      bcopy(host->h_addr, (char *) &myhostaddr, sizeof(myhostaddr));
      ipaddress = inet_ntoa(myhostaddr);
      domainname = (char *) host->h_name;
    }
  }
  uname(&kernel);
  strcpy(self.uname, username);
  strcpy(self.domain, domainname);
  strcpy(self.ip, ipaddress);
  sprintf(self.kernel, "%s %s %s", kernel.machine, kernel.sysname,
          kernel.release);
}

void welcome()
{
  wattron(win1, COLOR_PAIR(6));
  waddstr(win1, "========================== 歡迎來到  <");
  wattroff(win1, COLOR_PAIR(6));
  wattron(win1, COLOR_PAIR(4));
  wattron(win1, A_BOLD);
  waddstr(win1, "網路休閒大老２");
  wattroff(win1, A_BOLD);
  wattroff(win1, COLOR_PAIR(4));
  wattron(win1, COLOR_PAIR(6));
  waddstr(win1, "> ==========================");
  wattroff(win1, COLOR_PAIR(6));
  wrefresh(win1);
}

void change_color()
{
  if (color_mode == 0)
  {
    color_mode = 1;
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_WHITE, COLOR_BLACK);
    init_pair(8, COLOR_WHITE, COLOR_BLACK);
    xwaddstr(win1, "\n*** 牌之顏色模式切換為單色.", 0);
  } else
  {
    color_mode = 0;
    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    init_pair(2, COLOR_RED, COLOR_WHITE);
    init_pair(8, COLOR_BLACK, COLOR_WHITE);
    xwaddstr(win1, "\n*** 牌之顏色模式切換為彩色.", 0);
  }
}

void init_stuff()
{
  printf("\n\nChoose your terminfo type:\n");
  printf
      ("(1) linux (color supported)\n(2) vt100\n\nYou decide(default is 1):");
  scanf("%d", &term_type);
  if (term_type == 2)
    putenv("TERM=vt100");
  else
    putenv("TERM=linux");
  putenv("COLUMNS=80");
  putenv("LINES=24");
  use_env(TRUE);
  chdir("/tmp");
  initscr();
  start_color();
  init_pair(1, COLOR_BLACK, COLOR_WHITE);
  init_pair(2, COLOR_RED, COLOR_WHITE);
  init_pair(3, COLOR_WHITE, COLOR_BLUE);
  init_pair(4, COLOR_YELLOW, COLOR_BLACK);
  init_pair(5, COLOR_CYAN, COLOR_BLACK);
  init_pair(6, COLOR_GREEN, COLOR_BLACK);
  init_pair(7, COLOR_RED, COLOR_BLACK);
  init_pair(8, COLOR_BLACK, COLOR_WHITE);
  color_mode = 0;
  noecho();
  raw();
  win1 = newwin(22, 80, 0, 0);
  winc = newwin(1, 80, 22, 0);
  winu = newwin(1, 80, 23, 0);
  keypad(winu, TRUE);
  meta(winu, TRUE);
  wrefresh(winu);
  if (term_type == 2)
    wattrset(winc, A_REVERSE);
  else
  {
    wattrset(winc, COLOR_PAIR(3));
    wattron(winc, A_BOLD);
  }
  wrefresh(winc);
  idlok(win1, TRUE);
  scrollok(win1, TRUE);
  leaveok(win1, TRUE);
  welcome();
  condition(0);
  get_local_info();
}

void nclient_exit(n)
int n;
{
  condition(0);
  switch (n)
  {
  case 0:
    mvwprintw(winu, 0, 0, "%-80s", "*** 謝謝光臨, 再見囉!! ***");
    break;
  case 1:
    wattron(winu, COLOR_PAIR(7));
    mvwprintw(winu, 0, 0, "%-80s", "*** 請注意!! 系統遇到無法應付之錯誤");
    wattroff(winu, COLOR_PAIR(7));
    break;
  case 2:
    wattron(winu, COLOR_PAIR(4));
    wattron(winu, A_BOLD);
    mvwprintw(winu, 0, 0, "%-80s", "*** 密碼輸入錯誤三次, 強制離線! ***");
    wattroff(winu, A_BOLD);
    wattroff(winu, COLOR_PAIR(4));
    break;
  }
  mvwaddstr(winu, 0, 60, "*** 按任意鍵離開 ***");
  wrefresh(winu);
  wgetch(winu);
  clear();
  refresh();
  close(sockfd);
  endwin();
  exit(0);
}

int main(argc, argv)
int argc;
char *argv[];
{
  init_stuff();
  connect_server();
  login();
  main2();
}
