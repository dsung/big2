#include "big2.h"

#include <time.h>


int deskman,badsrvinfo,nn,card_go_out;
int last_n,pause_position,last_n_fake,pass_time,bet_times;
int tabkey_mode,beeper,arrow1,quitcheck,threecard,cardposx,queue[6],qlen,first_hand,idlemin,key_press,screen_wash;
int last_min,last_total_online,last_deskman,last_level,last_desk; /* to decrease the frequency of condition_win() refresh() */
char tmp2[1024],tmp3[1024],tmp4[1024],tmp5[1024],buffer2[3000],buffer3[3000],lastcmd[SCROLL_BACK_LINES][256];
char lastmsg[256],lastgivenick[11];

struct player self,o[3],g[4],finger,online;

struct card mycard[13],last[6];

int on_min,on_hr;
time_t now;

void
set_values()
{
int i;
self.on_game=0;
self.desk= -1;
self.level=0;
deskman= -1;
for(i=0;i<10;i++) bzero(lastcmd[i],sizeof(lastcmd[i]));
badsrvinfo=0;
pause_position=0;
tabkey_mode=0;
beeper=1;
quitcheck=0;
idlemin = -1;
screen_wash=0;
on_min = -1;
on_hr=0;
last_total_online = -1;
bzero(self.away_note,sizeof(self.away_note));
self.away_flag = 0;
}

main2()
{
initial_screen();
set_values();
for(;;) user_command();
}

new_write(fd,s,len)
int fd;
char *s;
size_t len;
{
write(fd,s,len);
}

initial_screen()
{
sprintf(tmp,"\n*** 登入成功\!!  歡迎 %s 來到網路休閒大老二.",self.nick);
sprintf(tmp2,"\n*** 伺服器啟動後第 %d 位登入  目前系統已註冊人數: %d",serial,total_register);
sprintf(tmp3,"\n*** 你是 %s@%s (%s)",self.uname,self.domain,self.ip);
sprintf(tmp4,"\n*** 金額: $%d  贏: %d  輸: %d  上站次數: %d",self.money,self.win,self.lose,self.login);
xwaddstr(win1,tmp,1);
xwaddstr(win1,tmp2,1);
xwaddstr(win1,tmp3,1);
xwaddstr(win1,tmp4,0);
}

writech(c)
char c;
{
write(sockfd,&c,1);
}

client_exit(n)
int n;
{
if (!n) {
if (self.on_game) {xwaddstr(win1,"\n*** 遊戲中是不准離開的!",0);
return;           }
xwaddstr(win1,"\n*** 謝謝光臨! 再見囉!!",1);
        }
if (n==1) xwaddstr(win1,"\n*** 密碼輸入錯誤次數太多啦!!! 你是個 cracker 嗎?! 別傻了 :p",1);
writech('q');
sprintf(tmp,"%80s","尚未連線");
xmvwaddstr(winc,0,0,tmp);
werase(winu);
xwaddstr(winu,"*** 按任意鍵離開 ***",0);
wrefresh(win1);
wrefresh(winc);
wgetch(winu);
clear();
refresh();
endwin();
close(sockfd);
exit(0);
}

error_exit()
{
xwaddstr(win1,"\n*** 請注意!! 系統發生了無法處理的錯誤.",0);
sprintf(tmp,"%80s","尚未連線");
xmvwaddstr(winc,0,0,tmp);
wrefresh(winc);
werase(winu);
xwaddstr(winu,"*** 按任意鍵離開 ***",0);
wgetch(winu);
clear();
refresh();
endwin();
close(sockfd);
exit(1);
}

xmvwaddstr(win,y,x,string)
int y,x;
WINDOW *win;
char *string;
{
wmove(win,y,x);
xwaddstr(win,string,1);
}

xwaddstr(win,string,flag)
WINDOW *win;
char *string;
int flag;
{
int i,j;
int cur_y,cur_x,max_y,max_x;
getmaxyx(win,max_y,max_x);
j=strlen(string);
for(i=0;i<j;i++) {
getyx(win,cur_y,cur_x);
if (cur_x==max_x-1) {
if (is_chinese_word(string,i)) waddch(win,'\n');}
waddch(win,string[i] & 0x000000ff); 
}
if (!flag) wrefresh(win);
}

is_chinese_word(s,x)
int x;
char *s;
{
int i,j;
unsigned char c,c2;
j=strlen(s);
for(i=0;i<j;i++) {
c=s[i];
c2=s[i+1];
if (is_big5(c,c2)) {if (i==x) return(1); i++;}
}
return(0);
}

void
condition(n)
int n;
{
switch(n) {
case 0:
mvwprintw(winc,0,0,"%80s","尚未連線");
wrefresh(winc);
break;
case 1:
mvwprintw(winc,0,0,"%80s","連線中");
wrefresh(winc);
break;
}
wrefresh(win1);
}    

condition_win()
{
int min,hour;
char c;
time(&now);
min=localtime(&now)->tm_min;
hour=localtime(&now)->tm_hour;
if (last_min!=min || last_total_online!=total_online || last_deskman!=deskman || last_level!=self.level || last_desk!=self.desk) {
if (last_min!=min) {on_min++; idlemin++; if (on_min==60) {on_min=0;on_hr++; if (on_hr==100) on_hr=0;}}
last_min=min;
last_total_online=total_online;
last_deskman=deskman;
last_level=self.level;
last_desk=self.desk;
werase(winc);
c=' ';
sprintf(tmp,"%80s"," ");
xmvwaddstr(winc,0,0,tmp);
wmove(winc,0,0);
if (self.level==100) c='#';
if (self.level==30) c='@';
if (self.desk==-1) 
sprintf(tmp,"[%02d:%02d] %c%s  尚未加入任何一桌  線上總人數:%03d",on_hr,on_min,c,self.nick,total_online);
else
sprintf(tmp,"[%02d:%02d] %c%s  桌號(%02d)  本桌人數:%03d  線上總人數:%03d",on_hr,on_min,c,self.nick,self.desk,deskman,total_online);
xwaddstr(winc,tmp,1);
xmvwaddstr(winc,0,60,"<若需輔助請打 /help>");
wrefresh(winc); 
}
}

user_command()
{
int i,j,k,l,m,nfds,argv1,a1,a2;
chtype c;
char cmd[256],cmd2[256],cmd_type_a[256],cmd_type[7];
fd_set rfds;
condition_win();
nfds=getdtablesize();
werase(winu);
wrefresh(winu);
bzero((char *)cmd,sizeof(cmd));
bzero((char *)cmd2,sizeof(cmd2));
for(i=0;i<154;i++) {
key_press=0;
for(;;) {
j=i%80;
wmove(winu,0,j);
if (i/80 && !j) werase(winu); 
wrefresh(winu);
FD_ZERO(&rfds);
FD_SET(0,&rfds);
FD_SET(sockfd,&rfds);
select(nfds,&rfds,(fd_set *)NULL,(fd_set *)NULL,(struct timeval *)NULL);
if (FD_ISSET(sockfd,&rfds)) read_info(); else break; }
key_press=1;
idlemin=0;
c=wgetch(winu);
if (c==3 && !quitcheck) {xwaddstr(win1,"\n*** 請再打一次 CTRL-C 確定離開.",0); if (beeper) beep(); quitcheck=1; i--; continue;}
if (c==4 && !quitcheck) {xwaddstr(win1,"\n*** 請再打一次 CTRL-D 確定離開.",0); if (beeper) beep(); quitcheck=1; i--; continue;}
if (c==3 || c==4) return(client_exit(0));
quitcheck=0;
if (c==12) {clearok(win1,TRUE); wrefresh(win1); clearok(win1,FALSE); i--; continue;}
if (pause_position==1 && !self.on_game) {sprintf(lastmsg,"*** 遊戲結束."); showcard_screen_end(0); pause_position=0; i--; continue;}
if (c==9 && self.on_game) {touchwin(winS); wattron(wins,COLOR_PAIR(6)); if (tabkey_mode) {xmvwaddstr(wins,1,13,"Talk"); tabkey_mode=0; xmvwaddstr(winB,4,0,"  "); mvwaddch(winB,4,2+(cardposx*4),' ');} else {xmvwaddstr(wins,1,13,"Card"); tabkey_mode=1; xmvwaddstr(winB,4,0,"→"); mvwaddch(winB,4,2+(cardposx*4),'*');} wattroff(wins,COLOR_PAIR(6)); wrefresh(winB); wrefresh(wins); i--; continue;}
if (tabkey_mode && self.on_game) {arrow_give_card(c); i--; continue;}
if (c=='\n' || i>=253) {cmd[i]='\0'; break;}
if (c==KEY_RIGHT) c=' ';
if (c==KEY_LEFT || c==KEY_BACKSPACE) c=127;
if (c==KEY_UP) c=16;
if (c==KEY_DOWN) c=14;
if (c==2) c=127;
if (c==6) c=' '; 
if (c==7) {xwaddstr(win1,"\n*** 嗶嗶?? 我怕吵啦! ^o^",0); i--; continue;}
if (c==16) {strcpy(cmd2,lastcmd[0]); for(l=0;l<SCROLL_BACK_LINES-1;l++) strcpy(lastcmd[l],lastcmd[l+1]); strcpy(lastcmd[SCROLL_BACK_LINES-1],cmd); strcpy(cmd,cmd2); l=strlen(cmd)%80; werase(winu); for(m=0;m<l;m++) mvwaddch(winu,0,m,cmd[80*(strlen(cmd)/80)+m] & 0x000000ff); wrefresh(winu); i=strlen(cmd)-1; continue;}
if (c==14) {strcpy(cmd2,lastcmd[SCROLL_BACK_LINES-1]); for(l=SCROLL_BACK_LINES-1;l>0;l--) strcpy(lastcmd[l],lastcmd[l-1]); strcpy(lastcmd[0],cmd); strcpy(cmd,cmd2); l=strlen(cmd)%80; werase(winu); for(m=0;m<l;m++) mvwaddch(winu,0,m,cmd[80*(strlen(cmd)/80)+m] & 0x000000ff); wrefresh(winu); i=strlen(cmd)-1; continue;}
if ((c=='' || c==127) && j) {mvwdelch(winu,0,j-1); wrefresh(winu); i-=2; continue;}
if ((c=='' || c==127) && !j) {
if (!i) {i--; continue;}
werase(winu);
for (k=0;k<79;k++)
mvwaddch(winu,0,k,cmd[k+(80*((i/80)-1))] & 0x000000ff);
wrefresh(winu);
i-=2;
continue;
}
cmd[i]=c;
mvwaddch(winu,0,j,c & 0x000000ff);
wrefresh(winu);
}
for(l=SCROLL_BACK_LINES-1;l>0;l--) strcpy(lastcmd[l],lastcmd[l-1]);
strcpy(lastcmd[0],cmd);
if (cmd[0]!='/' && self.on_game) return(give_card(cmd));
if (cmd[0]!='/') return(say_words(cmd));
if (i) {
if (strlen(cmd)==1) return;
a1=self.desk;
argv1=0;
a2=1;
sscanf(cmd,"/%6s%d",cmd_type_a,&argv1);
sscanf(cmd,"/%6s%d",cmd_type_a,&a1);
sscanf(cmd,"/%6s%d",cmd_type_a,&a2);
strcpy(cmd_type,cmd_type_a);
for(k=0;k<6;k++) cmd_type[k]=tolower(cmd_type[k]);
if (self.level==100) { /* SYSOP 專用 */
if (!strcmp(cmd_type,"exit")) return(exit_admin());
if (!strcmp(cmd_type,"kill")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(kill_user(cmd2));} 
if (!strcmp(cmd_type,"sendo")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(send_all(cmd2));}
if (!strcmp(cmd_type,"money")) {sscanf(cmd,"/money%s%d",cmd2,&m); return(money_tool(cmd2,m));}
if (!strcmp(cmd_type,"read")) {writech('A'); writech('r'); return;}
if (!strcmp(cmd_type,"write")) {writech('A'); writech('w'); return;}
if (!strcmp(cmd_type,"max")) {sprintf(buffer,"An%d\n",argv1); new_write(sockfd,buffer,strlen(buffer)); return;}
if (!strcmp(cmd_type,"down")) {sprintf(buffer,"Ad%d\n",argv1); new_write(sockfd,buffer,strlen(buffer)); return;}
if (!strcmp(cmd_type,"post1")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(announce(1,cmd2));}
if (!strcmp(cmd_type,"post2")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(announce(2,cmd2));}
if (!strcmp(cmd_type,"post3")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(announce(3,cmd2));}
if (!strcmp(cmd_type,"delay")) {sprintf(buffer,"Al%d\n",argv1); new_write(sockfd,buffer,strlen(buffer)); return;}
if (!strcmp(cmd_type,"error")) {sprintf(buffer,"AM%d\n",argv1); new_write(sockfd,buffer,strlen(buffer)); return;}
}
if (!strcmp(cmd_type,"date") || !strcmp(cmd_type,"d")) {char *date_time; time(&now); date_time=ctime(&now); date_time[strlen(date_time)-1]='\0'; sprintf(tmp,"\n*** 目前本地時刻: <%s>",date_time); xwaddstr(win1,tmp,0); return;}
if (!strcmp(cmd_type,"quit") || !strcmp(cmd_type,"q") || !strcmp(cmd_type,"bye")) return(client_exit(0));
if (!strcmp(cmd_type,"help") || !strcmp(cmd_type,"h")) return(helper(argv1));
if (!strcmp(cmd_type,"ver")  || !strcmp(cmd_type,"v")) return(ver());
if (!strcmp(cmd_type,"join") || !strcmp(cmd_type,"j")) return(join_desk(argv1));
if (!strcmp(cmd_type,"w") || !strcmp(cmd_type,"who")) return(who_list(a1,0));
if (!strcmp(cmd_type,"part") || !strcmp(cmd_type,"p") || !strcmp(cmd_type,"leave")) return(part_desk());
if (!strcmp(cmd_type,"me")|| !strcmp(cmd_type,"action")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(action(cmd2));}
if (!strcmp(cmd_type,"c") || !strcmp(cmd_type,"clear")) {werase(win1); xwaddstr(win1,"*** 螢幕清乾淨了!",0); return;}
if (!strcmp(cmd_type,"m"))   {sscanf(cmd,"/m%s",cmd2); strcpy(tmp,cmd2); m=strlen(cmd2); bzero(cmd2,sizeof(cmd2)); for(l=0;l<251;l++) cmd2[l]=cmd[4+m+l]; return(send_msg(tmp,cmd2));}
if (!strcmp(cmd_type,"color")) {return(change_color());}
if (!strcmp(cmd_type,"msg")) {sscanf(cmd,"/msg%s",cmd2); strcpy(tmp,cmd2); m=strlen(cmd2); bzero(cmd2,sizeof(cmd2)); for(l=0;l<249;l++) cmd2[l]=cmd[6+m+l]; return(send_msg(tmp,cmd2));}
if (!strcmp(cmd_type,"i") || !strcmp(cmd_type,"invite")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(invite(cmd2));} 
if (!strcmp(cmd_type,"f") || !strcmp(cmd_type,"finger") || !strcmp(cmd_type,"whois")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(finger_tool(cmd2));} 
if (!strcmp(cmd_type,"a") || !strcmp(cmd_type,"admin")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(admin(cmd2));}                    
if (!strcmp(cmd_type,"g") || !strcmp(cmd_type,"game")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(game_init(0,cmd2));} 
if (!strcmp(cmd_type,"gno3")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(game_init(1,cmd2));} 
if (!strcmp(cmd_type,"u") || !strcmp(cmd_type,"user")) return(user_list(0));
if (!strcmp(cmd_type,"r") || !strcmp(cmd_type,"rank")) return(rank_list(0,a2));
if (!strcmp(cmd_type,"l") || !strcmp(cmd_type,"list") || !strcmp(cmd_type,"table")) return(desk_list(0));
if (!strcmp(cmd_type,"away")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(change_away(cmd2));}                    
if (!strcmp(cmd_type,"passwd")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(change_passwd(cmd2));}                    
if (!strcmp(cmd_type,"t") || !strcmp(cmd_type,"topic")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(set_topic(cmd2));}
if (!strcmp(cmd_type,"beep") || !strcmp(cmd_type,"b")) {if (beeper) {xwaddstr(win1,"\n*** 喇叭聲音開關已經關上.",1); beeper=0;} else {xwaddstr(win1,"\n*** 喇叭聲音開關已經打開.",1); beep(); beeper=1;} return(wrefresh(win1));}
if (!strcmp(cmd_type,"o") || !strcmp(cmd_type,"oper")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(oper_cmd(cmd2));} 
if (!strcmp(cmd_type,"k") || !strcmp(cmd_type,"kill")) {m=strlen(cmd_type); for(l=0;l<155;l++) cmd2[l]=cmd[l+m+2]; return(kill_cmd(cmd2));} 
sprintf(cmd,"\n*** 抱歉, 系統無法辨認的指令: [%s]",cmd_type_a);
xwaddstr(win1,cmd,0);
}
}

helper(n)
int n;
{
switch(n) {
case 0:
xwaddstr(win1,"\n*** 輔助說明之目錄:                                      頁數: 0/6",1);
xwaddstr(win1,"\n/help 1                                   查詢基本指令(1)",1);
xwaddstr(win1,"\n/help 2                                   查詢基本指令(2)",1);
xwaddstr(win1,"\n/help 3                                   查詢基本指令(3)",1);
xwaddstr(win1,"\n/help 4                                   查詢基本玩牌指令及規則",1);
xwaddstr(win1,"\n/help 5                                   查詢特殊按鍵對照表",1);
if (self.level==100) 
xwaddstr(win1,"\n/help 6                                   系統管理者專用指令",1);
else 
xwaddstr(win1,"\n/help 6                                   作者小檔案",1);
xwaddstr(win1,"\n",1);
break;
case 1:
xwaddstr(win1,"\n*** 基本指令(1):                                         頁數: 1/6",1);
xwaddstr(win1,"\n/h  或 /help <頁數>                       查詢輔助說明      ",1);
xwaddstr(win1,"\n/q  或 /quit 或 /bye                      離開網路休閒大老二",1);
xwaddstr(win1,"\n/v  或 /ver                               查看有關版本的資訊",1);
xwaddstr(win1,"\n/j  或 /join <桌子編號>                   加入某一桌",1);
xwaddstr(win1,"\n/p  或 /part 或 /leave                    離開所在桌",1);
xwaddstr(win1,"\n/u  或 /user                              查看線上所有玩家",1);
xwaddstr(win1,"\n/w  或 /who <桌子編號>                    查看線上某一桌的玩家",1);
xwaddstr(win1,"\n/l  或 /list 或 /table                    查看線上所有有人的桌",1);
xwaddstr(win1,"\n/m  或 /msg <玩家> <悄悄話>               向某位玩家傳達秘密訊息",1);
xwaddstr(win1,"\n註: 1.打 /who -1 就可以看到所有閒置玩家囉(預設值為所在桌).",1);
xwaddstr(win1,"\n",1);
break;
case 2:
xwaddstr(win1,"\n*** 基本指令(2):                                         頁數: 2/6",1);
xwaddstr(win1,"\n/c  或 /clear                             清除訊息視窗",1);
xwaddstr(win1,"\n/f  或 /finger 或 /whois <玩家>           查詢某位玩家的詳細資料",1);
xwaddstr(win1,"\n/me 或 /action <動作>                     對本桌的其他人表示動作",1); 
xwaddstr(win1,"\n/a  或 /admin <特別密碼>                  變為伺服器系統管理者",1);
xwaddstr(win1,"\n*底下為桌長級以上使用指令*",1);
xwaddstr(win1,"\n/t  或 /topic <本桌附註>                  更改所在桌標題",1);
xwaddstr(win1,"\n/o  或 /oper <玩家>                       使某位玩家也變成桌長",1);
(win1,"\n註: 1.<桌子編號> 為 0 到 99 間任意一個數字",1);
xwaddstr(win1,"\n    2.桌長為每桌第一位加入者, 且他的名字前會多一個 \'@\' 號",1);
xwaddstr(win1,"\n",1);
break;
case 3:
xwaddstr(win1,"\n*** 基本指令(3):                                         頁數: 3/6",1);
xwaddstr(win1,"\n/d  或 /date                              顯示本地時刻",1);
xwaddstr(win1,"\n/r  或 /rank <名列>                       表列名人榜",1);
xwaddstr(win1,"\n/passwd <密碼>                            更改自己的密碼",1);
xwaddstr(win1,"\n/away <留言>                              當臨時離去可設定自動答話",1);
xwaddstr(win1,"\n註: 1. <密碼> 只能含英文字母和數字, 且最多 10 個字元",1);
xwaddstr(win1,"\n    2. <名列> 為開始排名的那個名次, 預設值為第 1 名",1);
xwaddstr(win1,"\n",1);
break;
case 4:
xwaddstr(win1,"\n*** 基本玩牌指令及規則:                                  頁數: 4/6",1);
xwaddstr(win1,"\n/i  或 /invite <玩家>                     邀請某位玩家加入所在桌",1);
xwaddstr(win1,"\n/k  或 /kill <玩家>                       當某一家閒置 > 3 min時可砍他",1);
xwaddstr(win1,"\n/b  或 /beep                              切換喇叭聲音開關",1);
xwaddstr(win1,"\n/color                                    切換牌色的顯示與否",1);
xwaddstr(win1,"\n*底下為桌長級以上使用指令*",1);
xwaddstr(win1,"\n/g  或 /game <對手1> <對手2> <對手3>      開始打一盤",1);
xwaddstr(win1,"\n/gno3 <對手1> <對手2> <對手3>             開始打一盤(沒有玩三條的)",1);
xwaddstr(win1,"\n*牌的大小規則*",1);
xwaddstr(win1,"\n２ > Ａ > Ｋ > Ｑ > Ｊ > 10 > ９ > ８ > ７ > ６ > ５ > ４ > ３",1);
xwaddstr(win1,"\nＳpade(黑桃) > Ｈeart(紅心) > Ｄiamond(方塊) > Ｃlub(梅花)",1);
xwaddstr(win1,"\n贏+=賭金倍數*(其餘三家剩牌總數) ; 輸-=賭金倍數*(自己剩餘牌數)",1);
xwaddstr(win1,"\n*順的規則(比尾巴那張,唯 23456 比 2)*",1);
xwaddstr(win1,"\n23456 > TJQKA > 9TJQK > 89TJQ > ... > 45678 > 34567 > A2345",1);
xwaddstr(win1,"\n^           ^       ^       ^             ^       ^       ^",1);
xwaddstr(win1,"\n",1);
break;
case 5:
xwaddstr(win1,"\n*** 特殊按鍵對照表:                                      頁數: 5/6",1);
xwaddstr(win1,"\nCTRL-L                                    重繪螢幕          ",1);
xwaddstr(win1,"\nCTRL-P 或 \"↑\"鍵                          往上捲回一行指令(最多 10 行)",1);
xwaddstr(win1,"\nCTRL-N 或 \"↓\"鍵                          往下捲回一行指令(最多 10 行)",1);
xwaddstr(win1,"\nCTRL-B 或 \"←\"鍵                          相當於 BackSpace 鍵",1);
xwaddstr(win1,"\nCTRL-F 或 \"→\"鍵                          相當於 Space 鍵",1);
xwaddstr(win1,"\nCTRL-C 或 CTRL-D                          相當於 /quit (需打兩次)",1);
xwaddstr(win1,"\nCTRL-J 或 CTRL-M                          相當於 Enter 鍵",1);
xwaddstr(win1,"\n*出牌模式下的對照*",1);
xwaddstr(win1,"\nCTRL-B 或 \"←\"鍵                          將游標往右移動一張牌",1);
xwaddstr(win1,"\nCTRL-F 或 \"→\"鍵                          將游標往左移動一張牌",1); 
xwaddstr(win1,"\nSPACE 鍵(或打相對應字母)                  選擇(切換)要出哪些牌",1);
xwaddstr(win1,"\nEnter 鍵                                  將牌打出去",1);
xwaddstr(win1,"\nTAB 鍵                                    切換出牌/交談模式",1);
xwaddstr(win1,"\n註: 1.每行指令最多可打 155 個字元",1);
xwaddstr(win1,"\n    2.打牌時要 PASS 就什麼也不選, 直接按 Enter",1);
xwaddstr(win1,"\n",1);
break;
case 6:
if (self.level==100) {
xwaddstr(win1,"\n*** 系統管理者專用指令:                                 頁數: 6/6",1);
xwaddstr(win1,"\n/exit                                     變回普通玩家",1);
xwaddstr(win1,"\n/kill <玩家>                              砍掉某位線上使用者",1);
xwaddstr(win1,"\n/sendo <話>                               對所有線上使用者廣播",1);
xwaddstr(win1,"\n/money <玩家> <錢數>                      更改某位線上玩家錢數",1);
xwaddstr(win1,"\n/read                                     切換系統記錄 readlog",1);
xwaddstr(win1,"\n/write                                    切換系統記錄 writelog",1);
xwaddstr(win1,"\n/max <人數>                               更改上線最大人數",1);
xwaddstr(win1,"\n/post1 <宣告事項>                         更改第一條宣告事項",1);
xwaddstr(win1,"\n/post2 <宣告事項>                         更改第二條宣告事項",1);
xwaddstr(win1,"\n/post3 <宣告事項>                         更改第三條宣告事項",1);
xwaddstr(win1,"\n/down <秒數>                              再 <秒數> 秒後停機廣播訊息!",1);
xwaddstr(win1,"\n/delay <seconds>                          設定讀取 client 之 TimeOut",1);
xwaddstr(win1,"\n/error <times>                            設定最大的 quit_cli2() 被呼叫次數",1);
xwaddstr(win1,"\n註: 當 <秒數> 為 -1 時會真的結束....",1);
xwaddstr(win1,"\n",1);
}
else {
xwaddstr(win1,"\n*** 作者小檔案:                                         頁數: 6/6",1);
xwaddstr(win1,"\n中央電機二Ａ 楊永瑞",1);
xwaddstr(win1,"\nE-mail: daniel@sparc41.dd.ncu.edu.tw",1);
xwaddstr(win1,"\n",1);
xwaddstr(win1,"\n作者曰: 如你有任何問題或建議, 請 mail 給我. :^)",1);
xwaddstr(win1,"\n",1);
}
break;
default:
sprintf(tmp,"\n*** 沒有頁數為 [%d] 的輔助說明.",n); 
xwaddstr(win1,tmp,1);
}
wrefresh(win1);
}

static char *ver_info[8]={
"(1) 省記憶體達 1/2 以上.",
"(2) 每盤上限 1,000,000 倍: 與賽者皆大於 $10,000,000.",
"    每盤上限   100,000 倍: 與賽者皆大於 $ 1,000,000.",
"    每盤上限    10,000 倍: 與賽者皆大於 $   100,000.",
"    除此之外, 每盤上限 1,000 倍: 與賽者有任一人小於 $100,000.",
"(3) 可用 /COLOR 切換牌種的顏色顯示.",
"(4) 取消 /dict 及 /user 指令的每次最多輸出 20 行的設計.",
"(5) 全面彩色化."
                         };
ver()
{
int i;
sprintf(tmp,"\n*** CLIENT VERSION: %s beta  本版更新如下:",CLIENT_VERSION);
xwaddstr(win1,tmp,1);
sprintf(tmp,"\n*** ");
for(i=0;i<8;i++) {
xwaddstr(win1,tmp,1);
xwaddstr(win1,ver_info[i],1);
}
wrefresh(win1);
}

action(string)
char *string;
{
char words[300];
if (!strlen(string)) return;
if (self.desk==-1) return;
sprintf(words,"Bb%s\n",string);
new_write(sockfd,words,strlen(words));
}

say_words(string)
char *string;
{
char words[300];
if (!strlen(string)) return;
if (self.desk==-1) return;
sprintf(words,"Bs%s\n",string);
new_write(sockfd,words,strlen(words));
}

send_all(string)
char *string;
{
if (!strlen(string)) return;
writech('A');
writech('b');
new_write(sockfd,string,strlen(string));
writech('\n');
}

join_desk(n)
int n;
{
if (n==self.desk) {
sprintf(tmp,"\n*** 你已經在第%02d桌了!",n);
xwaddstr(win1,tmp,0);
return; }
if (self.desk!=-1) {
sprintf(tmp,"\n*** 你得先離開你目前這一桌(第%02d桌)才行喔!",self.desk);
xwaddstr(win1,tmp,0);
return;
}
if (n>-1 && n<100) {
sprintf(buffer,"Bj%d#%s\n%s\n",n,username,domainname);
new_write(sockfd,buffer,strlen(buffer));
return; }
xwaddstr(win1,"\n*** 你給了一個無效的桌號.",0);
}

part_desk()
{
if (self.desk==-1) {
xwaddstr(win1,"\n*** 你尚未加入任何一桌.",0);
return;
}
if (self.on_game) {
xwaddstr(win1,"\n*** 遊戲中嘗試離開是無效的.",0);
return;
}
writech('B');
writech('p');
}

invite(nick)
char *nick;
{
if (self.desk==-1) {
xwaddstr(win1,"\n*** 你尚未加入任何一桌.",0);
return;
}
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你沒指定要邀請的人呀!",0);
return;
}
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",nick); 
xwaddstr(win1,buffer,0);
return;
}
sprintf(buffer,"Bi%s\n",nick);
new_write(sockfd,buffer,strlen(buffer));
}

finger_tool(nick)
char *nick;
{
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你尚未指定要查詢那個人.",0);
return;
}
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** %s 不是一個合法的玩家代號.",nick);
xwaddstr(win1,buffer,0);
return;
}
sprintf(buffer,"Bf%s\n",nick);
new_write(sockfd,buffer,strlen(buffer));
}

char *on_game_check(n)
int n;
{
if (n) return("是");
return("否");
}

user_list(n)
int n;
{
int i,j;
if (!n) {
sprintf(tmp,"Bu");
new_write(sockfd,tmp,strlen(tmp));
return; }
if (n==1) {
get_srvmsg(tmp,'#');
total_online = atoi(tmp);
xwaddstr(win1,"\n*** 玩家代號    桌號  遊戲中",1);
for(i=0;i<total_online;i++) {
get_srvmsg(buffer,'\n');
sscanf(buffer,"%s %d %d",online.nick,&(online.desk),&(online.on_game));
sprintf(buffer,"\n*** %-11s   %02d  %s",online.nick,online.desk,on_game_check(online.on_game));
xwaddstr(win1,buffer,1);
}
wrefresh(win1);
}
}

rank_list(n,rank)
int n,rank;
{
int i,j,k;
if (!n) {
sprintf(tmp,"Br%d\n",rank);
new_write(sockfd,tmp,strlen(tmp));
return; }
xwaddstr(win1,"\n*** 排名  玩家代號   錢數        贏    輸    登錄次數",1);
get_srvmsg(tmp,'\n');
get_srvmsg(buffer,'\n');
k=atoi(buffer)-1;
j=atoi(tmp);
for(i=0;i<j;i++) {
get_srvmsg(buffer,'\n');
sscanf(buffer,"%s %d %d %d %d %d",online.nick,&(online.money),&(online.win),&(online.lose),&(online.login));
sprintf(buffer,"\n*** %4d  %-10s $%-10d %-5d %-5d %d",i+1+k,online.nick,online.money,online.win,online.lose,online.login);
xwaddstr(win1,buffer,1);
}
wrefresh(win1);
}


who_list(n,type)
int n,type;
{
int i,j,k;
if (!type) {
if (n<-1 || n>99) {
xwaddstr(win1,"\n*** 你所要查看的桌號超過範圍喔!",0);
return;
}
sprintf(tmp,"Bw%d#",n);
new_write(sockfd,tmp,strlen(tmp));
return; }
get_srvmsg(tmp,'#');
k=atoi(tmp);
get_srvmsg(tmp2,'#');
j=atoi(tmp2);
if (!j) {
if (k==-1)
sprintf(buffer,"\n*** 現在線上沒有任何閒置的玩家.");
else
sprintf(buffer,"\n*** 抱歉! 第(%s)桌連一隻貓都沒有...",tmp);
xwaddstr(win1,buffer,0);
return;
}
if (k==-1)
sprintf(buffer,"\n*** 目前線上閒置玩家總人數: %s\n*** 玩家代號     遊戲中  贏     輸     金額         上站次數",tmp2);
else
sprintf(buffer,"\n*** 查看桌號:%s  該桌總人數:%s\n*** 玩家代號     遊戲中  贏     輸     金額         上站次數",tmp,tmp2);
xwaddstr(win1,buffer,1);
for(i=0;i<j;i++) {
get_srvmsg(buffer,'\n');
sscanf(buffer,"%s %d %d %d %d %d",online.nick,&(online.on_game),&(online.win),&(online.lose),&(online.money),&(online.login));
sprintf(buffer,"\n*** %-11s  %s      %-5d  %-5d  $%-10d  %-5d",online.nick,on_game_check(online.on_game),online.win,online.lose,online.money,online.login);
xwaddstr(win1,buffer,1);
}
wrefresh(win1);
}

desk_list(n)
int n;
{
int i,j,desk,better,ppl;
char topic[256];
if (!n) {
sprintf(tmp,"Bl");
new_write(sockfd,tmp,strlen(tmp));
return; }
get_srvmsg(tmp,'\n');
j=atoi(tmp); /* j desks alive (not include desk=-1) */
if (j) xwaddstr(win1,"\n*** 桌號  人數  賭金倍數   標題",1);
else
xwaddstr(win1,"\n*** 沒有任何一桌有人.",1);
for(i=0;i<j;i++) {
get_srvmsg(tmp,'\n');
get_srvmsg(topic,'\n');
sscanf(tmp,"%d %d %d",&desk,&ppl,&better);
sprintf(buffer,"\n***   %02d   %03d  %-5d      %s",desk,ppl,better,topic);
xwaddstr(win1,buffer,1);
}
wrefresh(win1);
}

set_topic(words)
char *words;
{
if (self.desk==-1) {
xwaddstr(win1,"\n*** 你還沒有加入任何一桌!",0);
return;
}
if (self.level<30) {
xwaddstr(win1,"\n*** 抱歉, 你的等級不夠.",0);
return;
}
if (!strlen(words)) {
xwaddstr(win1,"\n*** 你沒有指定要設的標題呀!",0);
return;
}
sprintf(buffer,"Bt%s\n",words);
new_write(sockfd,buffer,strlen(buffer));
}

is_goodpasswd(pass)
char *pass;
{
int i;
char c;
for (i=0;i<strlen(pass);i++) {
c=pass[i];
if (!((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97 && c<=122))) return(0);
}
return(1);
}

change_away(away)
char *away;
{
if (!strlen(away)) {
xwaddstr(win1,"\n*** 你不再是處於離去狀態.",0);
self.away_flag = 0;
return;
}
self.away_flag = 1;
xwaddstr(win1,"\n*** 你現在處於離去狀態.",0);
strcpy(self.away_note,away);
}

change_passwd(pass)
char *pass;
{
if (!strlen(pass)) {
xwaddstr(win1,"\n*** 你尚未給定任何欲更改的密碼.",0);
return;
}
if (strlen(pass)>10) {
xwaddstr(win1,"\n*** 所給的密碼太長, 最多只能有 10 個字元.",0);
return;
}
if (!is_goodpasswd(pass)) {
xwaddstr(win1,"\n*** 你給了一個不合法的密碼(只能有數字及字母)",0);
return;
}
sprintf(buffer,"Bn%s\n",pass);
new_write(sockfd,buffer,strlen(buffer));
}

admin(pass)
char *pass;
{
if (self.level==100) {
xwaddstr(win1,"\n*** 你早就是一位伺服器管理者了!",0);
return;
}
sprintf(buffer,"Ba%s\n",pass);
new_write(sockfd,buffer,strlen(buffer));
}

announce(n,words)
int n;
char *words;
{
sprintf(buffer,"Ap%d\n%s\n",n,words);
new_write(sockfd,buffer,strlen(buffer));
}

exit_admin()
{
sprintf(buffer,"Ae");
new_write(sockfd,buffer,strlen(buffer));
}

kill_user(nick)
char *nick;
{
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你沒指定要砍掉那位使用者啊!",0);
return;
}
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",nick); 
xwaddstr(win1,buffer,0);
return;
}
sprintf(buffer,"Ak%s\n",nick);
new_write(sockfd,buffer,strlen(buffer));
}

kill_cmd(nick)
char *nick;
{
if (!self.on_game) {
xwaddstr(win1,"\n*** 你尚未進行遊戲.",0);
return;
}
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你沒有指定要踢的人呀!",0);
return;
}
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",nick); 
xwaddstr(win1,buffer,0);
return;
}
if (strcmp(nick,o[0].nick) && strcmp(nick,o[1].nick) && strcmp(nick,o[2].nick)) {
sprintf(buffer,"\n*** 你所指定的 %s 不屬於對手中的任何一位.",nick);
xwaddstr(win1,buffer,0);
return;
}
sprintf(buffer,"Bk%s\n",nick);
new_write(sockfd,buffer,strlen(buffer));
}

oper_cmd(nick)
char *nick;
{
if (self.desk==-1) {
xwaddstr(win1,"\n*** 你還沒有加入任何一桌!",0);
return;
}
if (self.level<30) {
xwaddstr(win1,"\n*** 抱歉, 你的等級不夠.",0);
return;
}
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你沒有指定要使他變成桌長的人呀!",0);
return;
}
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",nick); 
xwaddstr(win1,buffer,0);
return;
}
if (!strcmp(nick,self.nick)) {
xwaddstr(win1,"\n*** 你已經是一位桌長了!",0);
return;
}
sprintf(buffer,"Bo%s\n",nick);
new_write(sockfd,buffer,strlen(buffer));
}

money_tool(who,money)
int money;
char *who;
{
if (!strlen(who)) {
xwaddstr(win1,"\n*** 你沒指定要改變誰的錢數呀 :D",0);
return;
}
if (!is_goodnick(who)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",who); 
xwaddstr(win1,buffer,0);
return;
} 
sprintf(buffer,"Am%s\n%d#",who,money);
new_write(sockfd,buffer,strlen(buffer));
}

send_msg(nick,words)
char *nick,*words;
{
if (!strlen(nick)) {
xwaddstr(win1,"\n*** 你沒講要把訊息傳給誰呀!",0);
return;
}
if (!strlen(words)) return;
if (!is_goodnick(nick)) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",nick); 
xwaddstr(win1,buffer,0);
return;
}
sprintf(buffer,"Bm%s\n%s\n",nick,words);
new_write(sockfd,buffer,strlen(buffer));
}

is_goodnick(nick)
char *nick;
{
int i;
char c;
if (!strlen(nick)) return(0);
for(i=0;i<strlen(nick);i++) {
c=nick[i];
if (!((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97 && c<=122))) return(0); }
if (i>10) return(0); else return(1); 
}
