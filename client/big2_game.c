#include "big2.h"

game_init(type,opponents)
int type;
char *opponents;
{
int i;
char o[3][11];
if (self.desk==-1) {
xwaddstr(win1,"\n*** 你還沒加入任何一桌.",0);
return;
}
if (self.on_game) {
xwaddstr(win1,"\n*** 你已經在進行遊戲了喔!",0);
return;
}
sscanf(opponents,"%s%s%s",o[0],o[1],o[2]);
if (!strlen(o[2]) || !strlen(o[1]) || !strlen(o[0])) {
xwaddstr(win1,"\n*** 所給定的對手人數不足!",0);
return;
}
for(i=0;i<3;i++) {if (!is_goodnick(o[i])) {
sprintf(buffer,"\n*** 那個 %s 並不是一個合法的玩家代號喔!",o[i]); 
xwaddstr(win1,buffer,0);
return;
}
if (!strcmp(o[i],self.nick)) {
xwaddstr(win1,"\n*** 你一個人不能玩兩個以上喔!",0);
return;
}
}
if (!strcmp(o[0],o[1]) || !strcmp(o[0],o[2]) || !strcmp(o[1],o[2])) {
xwaddstr(win1,"\n*** 你不能跟兩個以上相同的人玩喔!",0);
return;
}
if (self.level<30) {
xwaddstr(win1,"\n*** 你的等級不夠喔!",0);
return;
}
if (!type) sprintf(buffer,"Gi3%s\n%s\n%s\n%d\n",o[0],o[1],o[2],(int)getpid());
if (type==1) sprintf(buffer,"Gi0%s\n%s\n%s\n%d\n",o[0],o[1],o[2],(int)getpid()); /* no 3 條 */
new_write(sockfd,buffer,strlen(buffer));
}

convert1(c)
char c;
{
if (c=='A') return(2);
if (c=='B') return(1);
if (c=='C') return(0);
if (c=='D') return(12);
if (c=='E') return(11);
if (c=='F') return(10);
if (c=='G') return(9);
if (c=='H') return(8);
if (c=='I') return(7);
if (c=='J') return(6);
if (c=='K') return(5);
if (c=='L') return(4);
if (c=='M') return(3);
}

char convert3(n)
int n;
{    
if (n==2) return('A');
if (n==1) return('B');
if (n==0) return('C');
if (n==12) return('D');
if (n==11) return('E');
if (n==10) return('F');
if (n==9) return('G');
if (n==8) return('H');
if (n==7) return('I');
if (n==6) return('J');
if (n==5) return('K');
if (n==4) return('L');
if (n==3) return('M');
}

char style(n)
int n;
{
if (0 <= n && n <= 12) return('S');
if (13 <= n && n <= 25) return('H');
if (26 <= n && n <= 38) return('D');
if (39 <= n && n <= 51) return('C');
}

get_srvcards(crd)
struct card *crd;
{
int m,n;
char c;
for(m=n=0;;) {
read(sockfd,&c,1);
if (c=='\n') break;
if (!m) {
crd[n].point=convert1(c);
m=1; }
else {
crd[n].suit=c;
m=0;
n++;}
}
return(n);
}

sort_cards(n)
{
int i,j,k,l;
struct card match,tmp[52];
for(k=i=0;i<13;i++) {
match.point=(15-i)%13;
for(l=0;l<40;l+=13) {
match.suit=style(l);
for(j=0;j<n;j++) 
if (mycard[j].point==match.point && mycard[j].suit==match.suit) {
tmp[k].point=match.point;
tmp[k].suit=match.suit;
k++; }
}}
for(i=0;i<k;i++) {
mycard[i].point=tmp[i].point;
mycard[i].suit=tmp[i].suit; }
}

showcard_screen_init(n)
int n;
{
werase(win1);
wrefresh(win1);
if (!n) {
if (!screen_wash) {
screen_wash=1;
delwin(win1);
winA=newwin(13,36,0,0);
winC=newwin(4,54,13,0);
winB=newwin(5,54,17,0);
wins=newwin(9,26,13,54);
winS=subwin(wins,4,22,17,56);
win0=newwin(1,44,0,36);
win2=newwin(12,44,1,36);
win1=newwin(11,39,1,38);
}
werase(winA);
werase(winC);
werase(winB);
werase(wins);
werase(winS);
werase(win0);
werase(win2);
werase(win1);
xmvwaddstr(wins,0,0,"※TAB 鍵切換交談/出牌模式");
xmvwaddstr(wins,1,0,"◎目前模式: <");
wattron(wins,COLOR_PAIR(6));
xwaddstr(wins,"Talk");
wattroff(wins,COLOR_PAIR(6));
xwaddstr(wins,"> 模式");
xmvwaddstr(wins,2,0,"◎最後出牌者:"); 
xmvwaddstr(wins,3,0,"╭───────────╮");
xmvwaddstr(wins,4,0,"│                      │");
xmvwaddstr(wins,5,0,"│                      │");
xmvwaddstr(wins,6,0,"│                      │");
xmvwaddstr(wins,7,0,"│                      │");
xmvwaddstr(wins,8,0,"╰───────────╯");
sprintf(tmp,"玩家代號:%-10s  金額: $%-10d",self.nick,self.money);
xmvwaddstr(winC,0,0,tmp);
sprintf(tmp,"玩家代號:%-10s  金額: $%-10d",o[0].nick,o[0].money);
xmvwaddstr(winC,1,0,tmp);
sprintf(tmp,"玩家代號:%-10s  金額: $%-10d",o[1].nick,o[1].money);
xmvwaddstr(winC,2,0,tmp);
sprintf(tmp,"玩家代號:%-10s  金額: $%-10d",o[2].nick,o[2].money);
xmvwaddstr(winC,3,0,tmp);
 xmvwaddstr(win0,0,0,"�歈�����������������������������������������");
 xmvwaddstr(win2,0,0,"��                                        ��");
 xmvwaddstr(win2,1,0,"��                                        ��");
 xmvwaddstr(win2,2,0,"��                                        ��");
 xmvwaddstr(win2,3,0,"��                                        ��");
 xmvwaddstr(win2,4,0,"��                                        ��");
 xmvwaddstr(win2,5,0,"��                                        ��");
 xmvwaddstr(win2,6,0,"��                                        ��");
 xmvwaddstr(win2,7,0,"��                                        ��");
 xmvwaddstr(win2,8,0,"��                                        ��");
 xmvwaddstr(win2,9,0,"��                                        ��");
xmvwaddstr(win2,10,0,"��                                        ��");
xmvwaddstr(win2,11,0,"�裺�����������������������������������������");
wrefresh(win0);
wrefresh(win2);
werase(win1);
scrollok(win1,TRUE);
idlok(win1,FALSE);
xwaddstr(win1,lastmsg,1);
wrefresh(winA);
wrefresh(winB);
wrefresh(win1);
wrefresh(wins);
wrefresh(winC);
}
if (n==1) {
delwin(win1);
winA=newwin(13,36,0,0);
win0=newwin(13,44,0,36);
win1=newwin(9,80,13,0);
winS=newwin(4,22,7,40);
 xmvwaddstr(win0,0,0,"�歈�����������������������������������������");
sprintf(tmp,"�曭戛a代號:%-10s  金額: $%-10d  ��",g[0].nick,g[0].money);
xmvwaddstr(win0,1,0,tmp);
sprintf(tmp,"�曭戛a代號:%-10s  金額: $%-10d  ��",g[1].nick,g[1].money);
xmvwaddstr(win0,2,0,tmp);
sprintf(tmp,"�曭戛a代號:%-10s  金額: $%-10d  ��",g[2].nick,g[2].money);
xmvwaddstr(win0,3,0,tmp);
sprintf(tmp,"�曭戛a代號:%-10s  金額: $%-10d  ��",g[3].nick,g[3].money);
xmvwaddstr(win0,4,0,tmp);
 xmvwaddstr(win0,5,0,"�瓥怮嵽X牌者:                             ��");
 xmvwaddstr(win0,6,0,"�齰~────────────╮            ��");
 xmvwaddstr(win0,7,0,"�齰x                        │            ��");
 xmvwaddstr(win0,8,0,"�齰x                        │            ��");
 xmvwaddstr(win0,9,0,"�齰x                        │            ��");
xmvwaddstr(win0,10,0,"�齰x                        │            ��");
xmvwaddstr(win0,11,0,"�齰╰w───────────╯            ��");
xmvwaddstr(win0,12,0,"�裺�����������������������������������������");
wrefresh(win0);
wrefresh(winA);
werase(win1);
scrollok(win1,TRUE);
idlok(win1,TRUE);
xwaddstr(win1,lastmsg,1);
wrefresh(win1);
wrefresh(winS);
}
pause_position=0;
}

showcard_screen_end(n)
int n;
{
if (!n) {
delwin(win1);
delwin(win2);
delwin(win0);
delwin(winS);
delwin(wins);
delwin(winB);
delwin(winC);
delwin(winA);
}
if (n==1) {
delwin(winS);
delwin(win1);
delwin(win0);
delwin(winA);
}
win1=newwin(22,80,0,0);
scrollok(win1,TRUE);
idlok(win1,TRUE);
clearok(win1,TRUE);
werase(win1);
clearok(win1,FALSE);
xwaddstr(win1,lastmsg,0);
screen_wash=0;
}

char *convert4(n)
int n;
{
if (n==2) return("２");
if (n==1) return("Ａ");
if (n==0) return("Ｋ");
if (n==12) return("Ｑ");
if (n==11) return("Ｊ");
if (n==10) return("10");
if (n==9) return("９");
if (n==8) return("８");
if (n==7) return("７");
if (n==6) return("６");
if (n==5) return("５");
if (n==4) return("４");
if (n==3) return("３");
}

char *convert5(c)
char c;
{
if (c=='S') return("Ｓ");
if (c=='H') return("Ｈ");
if (c=='D') return("Ｄ");
if (c=='C') return("Ｃ");
}

show_cards(n,m)
int n,m;
{
int i;
int yy,xx;
if (!n) {
yy=0;
xx=0;
werase(winA);
wattron(winA,COLOR_PAIR(4));
wattron(winA,A_BOLD);
for(i=0;i<self.cardleft;i++) {xmvwaddstr(winA,11,5+(i*2),"▉"); xmvwaddstr(winA,12,5+(i*2),"▉");}
for(i=0;i<o[0].cardleft;i++) xmvwaddstr(winA,12-i,32,"▇▇");
for(i=0;i<o[1].cardleft;i++) {xmvwaddstr(winA,0,29-(i*2),"▉"); xmvwaddstr(winA,1,29-(i*2),"▉");}
for(i=0;i<o[2].cardleft;i++) xmvwaddstr(winA,i,0,"▇▇");
wattroff(winA,A_BOLD);
wattroff(winA,COLOR_PAIR(4));
xmvwaddstr(winA,10,5+((26-strlen(self.nick))/2),self.nick);
sprintf(tmp,"%10s",o[0].nick); xmvwaddstr(winA,6,22,tmp);
xmvwaddstr(winA,2,5+((26-strlen(o[1].nick))/2),o[1].nick);
xmvwaddstr(winA,6,4,o[2].nick);
wattron(winA,COLOR_PAIR(5));
xmvwaddstr(winA,6,17,"◇");
if (self.on_game==1) {xmvwaddstr(winA,7,17,"↓"); if(beeper) beep();}
if (self.on_game==2) xmvwaddstr(winA,6,15,"←");
if (self.on_game==3) xmvwaddstr(winA,5,17,"↑");
if (self.on_game==4) xmvwaddstr(winA,6,19,"→");
wattroff(winA,COLOR_PAIR(5));
wrefresh(winA);
if (m) {
werase(winB);
if (last_n_fake) wrefresh(winB);
for(i=0;i<self.cardleft;i++) {
if (!i) {
wattron(winB,COLOR_PAIR(8));
xmvwaddstr(winB,yy,xx,"┌"); xmvwaddstr(winB,yy+1,xx,"│"); xmvwaddstr(winB,yy+2,xx,"│"); xmvwaddstr(winB,yy+3,xx,"└");
wattroff(winB,COLOR_PAIR(8));
}
else {
wattron(winB,COLOR_PAIR(8));
xmvwaddstr(winB,yy,xx+(4*i),"┬"); xmvwaddstr(winB,yy+1,xx+(4*i),"│"); xmvwaddstr(winB,yy+2,xx+(4*i),"│"); xmvwaddstr(winB,yy+3,xx+(4*i),"┴");
wattroff(winB,COLOR_PAIR(8));
}
wattron(winB,COLOR_PAIR(8));
xmvwaddstr(winB,yy,xx+(4*i)+2,"─");
wattroff(winB,COLOR_PAIR(8));
if (mycard[i].suit=='S' || mycard[i].suit=='C')
wattron(winB,COLOR_PAIR(1)); else wattron(winB,COLOR_PAIR(2));
xmvwaddstr(winB,yy+1,xx+(4*i)+2,convert4(mycard[i].point));
xmvwaddstr(winB,yy+2,xx+(4*i)+2,convert5(mycard[i].suit));
if (mycard[i].suit=='S' || mycard[i].suit=='C')
wattroff(winB,COLOR_PAIR(1)); else wattroff(winB,COLOR_PAIR(2));
wattron(winB,COLOR_PAIR(8));
xmvwaddstr(winB,yy+3,xx+(4*i)+2,"─");
wattroff(winB,COLOR_PAIR(8));
sprintf(tmp," %c",65+i);
xmvwaddstr(winB,yy+4,xx+(4*i)+2,tmp);
}
wattron(winB,COLOR_PAIR(8));
if (self.cardleft) {
xmvwaddstr(winB,yy,xx+(4*i),"┐");
xmvwaddstr(winB,yy+1,xx+(4*i),"│");
xmvwaddstr(winB,yy+2,xx+(4*i),"│");
xmvwaddstr(winB,yy+3,xx+(4*i),"┘");
wattroff(winB,COLOR_PAIR(8));
}
wrefresh(winB);
}
} /* if (!n) */
if (n==1) {
werase(winA);
wattron(winA,COLOR_PAIR(4));
wattron(winA,A_BOLD);
for(i=0;i<g[0].cardleft;i++) {xmvwaddstr(winA,11,5+(i*2),"▉"); xmvwaddstr(winA,12,5+(i*2),"▉");}
for(i=0;i<g[1].cardleft;i++) xmvwaddstr(winA,12-i,32,"▇▇");
for(i=0;i<g[2].cardleft;i++) {xmvwaddstr(winA,0,29-(i*2),"▉"); xmvwaddstr(winA,1,29-(i*2),"▉");}
for(i=0;i<g[3].cardleft;i++) xmvwaddstr(winA,i,0,"▇▇");
wattroff(winA,A_BOLD);
wattroff(winA,COLOR_PAIR(4));
xmvwaddstr(winA,10,5+((26-strlen(g[0].nick))/2),g[0].nick);
sprintf(tmp,"%10s",g[1].nick); xmvwaddstr(winA,6,22,tmp);
xmvwaddstr(winA,2,5+((26-strlen(g[2].nick))/2),g[2].nick);
xmvwaddstr(winA,6,4,g[3].nick);
wattron(winA,COLOR_PAIR(5));
xmvwaddstr(winA,6,17,"◇");
if (arrow1==0) xmvwaddstr(winA,7,17,"↓");
if (arrow1==3) xmvwaddstr(winA,6,15,"←");
if (arrow1==2) xmvwaddstr(winA,5,17,"↑");
if (arrow1==1) xmvwaddstr(winA,6,19,"→");
wattroff(winA,COLOR_PAIR(5));
wrefresh(winA);
} /* if (n==1) */
}

is_unique(a,b)
int *a,b;
{
int i,j;
for(i=0;i<b;i++) {
if (a[i] == 0 && b != 1) return(2);
for(j=0;j<b;j++) 
if (a[i] == a[j] && i != j) return(1);
}
return(0);
}

see_card(kind,n)
int kind,n;
{
char who[11];
int i;
int xx,yy;
yy=0;
xx=0;
werase(winS);
if (!kind) { /* others give */
if (self.on_game==2) strcpy(who,o[2].nick);
if (self.on_game==3) strcpy(who,o[1].nick);
if (self.on_game==4) strcpy(who,o[0].nick);
if (n) {wrefresh(winS); last_n=n; pass_time=0; sprintf(lastgivenick,"%-10s",who);}
if (last_n) {
wattron(wins,COLOR_PAIR(6));
xmvwaddstr(wins,2,14,lastgivenick);
wattroff(wins,COLOR_PAIR(6));
wrefresh(wins);
for(i=0;i<last_n;i++) {
if (!i) {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx,"┌"); xmvwaddstr(winS,yy+1,xx,"│"); xmvwaddstr(winS,yy+2,xx,"│"); xmvwaddstr(winS,yy+3,xx,"└");
wattroff(winS,COLOR_PAIR(8));
}
else {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx+(4*i),"┬"); xmvwaddstr(winS,yy+1,xx+(4*i),"│"); xmvwaddstr(winS,yy+2,xx+(4*i),"│"); xmvwaddstr(winS,yy+3,xx+(4*i),"┴");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
if (last[i].suit=='S' || last[i].suit=='C')
wattron(winS,COLOR_PAIR(1)); else wattron(winS,COLOR_PAIR(2));
xmvwaddstr(winS,yy+1,xx+(4*i)+2,convert4(last[i].point));
xmvwaddstr(winS,yy+2,xx+(4*i)+2,convert5(last[i].suit));
if (last[i].suit=='S' || last[i].suit=='C')
wattroff(winS,COLOR_PAIR(1)); else wattroff(winS,COLOR_PAIR(2));
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy+3,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx+(4*i),"┐");
xmvwaddstr(winS,yy+1,xx+(4*i),"│");
xmvwaddstr(winS,yy+2,xx+(4*i),"│");
xmvwaddstr(winS,yy+3,xx+(4*i),"┘");
wattroff(winS,COLOR_PAIR(8));
} 
if (!n) {
pass_time++;
sprintf(tmp,"\n*** %s 放棄本次出牌.",who,pass_time);
xwaddstr(win1,tmp,1);
if (pass_time==3) xwaddstr(win1,"\n*** 三家都 PASS 囉, 你可以出任意牌型.",1);
}
wrefresh(win1);
wrefresh(winS);
return;
}

if (kind==1) { /* self give */
card_go_out = 0;
if (n) {wrefresh(winS); last_n=n; pass_time=0; strcpy(lastgivenick,"你自己    ");}
if (last_n) {
wattron(wins,COLOR_PAIR(6));
xmvwaddstr(wins,2,14,lastgivenick);
wattroff(wins,COLOR_PAIR(6));
wrefresh(wins);
for(i=0;i<last_n;i++) {
if (!i) {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx,"┌"); xmvwaddstr(winS,yy+1,xx,"│"); xmvwaddstr(winS,yy+2,xx,"│"); xmvwaddstr(winS,yy+3,xx,"└");
wattroff(winS,COLOR_PAIR(8));
}
else {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,(4*i)+xx,"┬"); xmvwaddstr(winS,yy+1,(4*i)+xx,"│"); xmvwaddstr(winS,yy+2,(4*i)+xx,"│"); xmvwaddstr(winS,yy+3,(4*i)+xx,"┴");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
if (last[i].suit=='S' || last[i].suit=='C')
wattron(winS,COLOR_PAIR(1)); else wattron(winS,COLOR_PAIR(2));
xmvwaddstr(winS,yy+1,xx+(4*i)+2,convert4(last[i].point));
xmvwaddstr(winS,yy+2,xx+(4*i)+2,convert5(last[i].suit));
if (last[i].suit=='S' || last[i].suit=='C')
wattroff(winS,COLOR_PAIR(1)); else wattroff(winS,COLOR_PAIR(2));
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy+3,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,(4*i)+xx,"┐");
xmvwaddstr(winS,yy+1,(4*i)+xx,"│");
xmvwaddstr(winS,yy+2,(4*i)+xx,"│");
xmvwaddstr(winS,yy+3,(4*i)+xx,"┘");
wattroff(winS,COLOR_PAIR(8));
} 
if (!n) xwaddstr(win1,"\n*** 你放棄本次出牌.",1);
wrefresh(winS);
wrefresh(win1);
return;
}

if (kind==2) { /* view others' give ; self.on_game = 0 */
if (n) {wrefresh(winS); last_n=n; sprintf(lastgivenick,"%10s",g[arrow1].nick);}
if (last_n) {
wattron(win0,COLOR_PAIR(6));
xmvwaddstr(win0,5,14,lastgivenick);
wattroff(win0,COLOR_PAIR(6));
wrefresh(win0);
for(i=0;i<last_n;i++) {
if (!i) {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx,"┌"); xmvwaddstr(winS,yy+1,xx,"│"); xmvwaddstr(winS,yy+2,xx,"│"); xmvwaddstr(winS,yy+3,xx,"└");
wattroff(winS,COLOR_PAIR(8));
}
else {
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,(4*i)+xx,"┬"); xmvwaddstr(winS,yy+1,(4*i)+xx,"│"); xmvwaddstr(winS,yy+2,(4*i)+xx,"│"); xmvwaddstr(winS,yy+3,(4*i)+xx,"┴");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
if (last[i].suit=='S' || last[i].suit=='C')
wattron(winS,COLOR_PAIR(1)); else wattron(winS,COLOR_PAIR(2));
xmvwaddstr(winS,yy+1,xx+(4*i)+2,convert4(last[i].point));
xmvwaddstr(winS,yy+2,xx+(4*i)+2,convert5(last[i].suit));
if (last[i].suit=='S' || last[i].suit=='C')
wattroff(winS,COLOR_PAIR(1)); else wattroff(winS,COLOR_PAIR(2));
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy+3,xx+(4*i)+2,"─");
wattroff(winS,COLOR_PAIR(8));
}
wattron(winS,COLOR_PAIR(8));
xmvwaddstr(winS,yy,(4*i)+xx,"┐");
xmvwaddstr(winS,yy+1,(4*i)+xx,"│");
xmvwaddstr(winS,yy+2,(4*i)+xx,"│");
xmvwaddstr(winS,yy+3,(4*i)+xx,"┘");
wattroff(winS,COLOR_PAIR(8));
} 
if (!n) {
sprintf(tmp,"\n*** %s 放棄本次出牌.",g[arrow1].nick);
xwaddstr(win1,tmp,1); }
wrefresh(winS);
wrefresh(win1);
return;
}
}

check_win_or_lose(n)
int n;
{
int winner;
winner= -1;
if (!n) {
if (!o[0].cardleft) winner=0;
if (!o[1].cardleft) winner=1;
if (!o[2].cardleft) winner=2;
if (!self.cardleft) { /* you win !! */
xwaddstr(win1,"\n*** 恭喜! 你贏了!\n*** 按任意鍵繼續",0);
xmvwaddstr(winB,0,0,"┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐");
xmvwaddstr(winB,1,0,"│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│");
xmvwaddstr(winB,2,0,"│▽│▽│▽│▽│▽│▽│▽│▽│▽│▽│▽│▽│▽│");
xmvwaddstr(winB,3,0,"└─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘");
xmvwaddstr(winB,4,0,"作者說: 呵呵...你真是天生贏家!! 太厲害囉 :D           ");
wrefresh(winB);
self.money+=((o[0].cardleft+o[1].cardleft+o[2].cardleft)*bet_times);
self.win+=1;
self.on_game=0;
pause_position=1;
return;
}
if (winner!=-1) {
sprintf(tmp,"\n*** 可惜... 被 \"%s\" 贏了這場!\n*** 按任意鍵繼續",o[winner].nick);
xwaddstr(win1,tmp,0);
xmvwaddstr(winB,0,0,"┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐");
xmvwaddstr(winB,1,0,"│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│‥│");
xmvwaddstr(winB,2,0,"│∩│∩│∩│∩│∩│∩│∩│∩│∩│∩│∩│∩│∩│");
xmvwaddstr(winB,3,0,"└─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘");
xmvwaddstr(winB,4,0,"作者說: 什麼? 輸了? 別氣餒! 再來一盤! 幹掉他吧!!      ");
wrefresh(winB);
self.money-=(self.cardleft*bet_times);
if (self.money<=0) {self.money=500; xwaddstr(win1,"\n*** 哎呀!! 你破產了.\n*** 系統補助救濟金 $500 銀元給你!",0);}
self.lose+=1;
self.on_game=0;
pause_position=1;
return;
}
} /* if (!n) */
if (n==1) {
if (!g[0].cardleft) winner=0;
if (!g[1].cardleft) winner=1;
if (!g[2].cardleft) winner=2;
if (!g[3].cardleft) winner=3;
if (winner!=-1) {
sprintf(lastmsg,"比賽結束.");
showcard_screen_end(1);
sprintf(tmp,"\n*** \"%s\" 贏了這場!!",g[winner].nick);
xwaddstr(win1,tmp,0);
return;
}
}
}

give_card(cards)
char *cards;
{
int m,n,argc,a[6];
if (!tabkey_mode) return(say_words(cards)); /* talk position */
if (self.on_game!=1) {
xwaddstr(win1,"\n*** 還沒輪到你打! 別急!",0);
return;
}
if (card_go_out) {
xwaddstr(win1,"\n*** 別急.... 按 Enter 一下就夠了.",0);
return;
}
argc=sscanf(cards,"%d%d%d%d%d%d",&a[0],&a[1],&a[2],&a[3],&a[4],&a[5]);
if (!argc) {xwaddstr(win1,"\n*** 請鍵入一些數字來出牌! (如: 1 2 3 則出第 1, 2, 3 張牌).",0); return;}
if (argc==4) {xwaddstr(win1,"\n*** 大老二裡沒有給四張的啦!",0); return;}
if (argc==6) {xwaddstr(win1,"\n*** 你一次最多不能給超過五張牌!",0); return;}
if (argc==-1) {xwaddstr(win1,"\n*** 我實在搞不清楚你到底要出什麼!",0); return;}
for(n=0;n<argc;n++) if (a[n]>self.cardleft || a[n]<0) {sprintf(tmp,"\n*** 你出的第%d張牌超出範圍了!",n+1); xwaddstr(win1,tmp,0); return;}
if (is_unique(&a[0],argc)==1) {xwaddstr(win1,"\n*** 你給的牌裡頭有重覆的!",0); return;}
if (is_unique(&a[0],argc)==2) {xwaddstr(win1,"\n*** 你不能夠同時放棄出牌又要給牌!",0); return;}
if (!a[0]) { /* pass !! */
if (pass_time==3) {
xwaddstr(win1,"\n*** 已經 PASS 一輪囉, 不能再 PASS 啦!",0);
return;
}
if (first_hand) {
xwaddstr(win1,"\n*** 第一次出牌不能打 PASS!",0);
return;
}
sprintf(tmp,"Gg%s\n%s\n%s\n%d#",o[0].nick,o[1].nick,o[2].nick,0);
new_write(sockfd,tmp,strlen(tmp));
return;
}
if (pass_time==3) last_n=0;
for(n=0;n<argc;n++) for(m=n+1;m<argc;m++) if (a[n]>a[m]) {a[5]=a[n]; a[n]=a[m]; a[m]=a[5];} /* make the cards sorted */
/* the real ruler */
switch(big2rule(a,argc)) {
case 0:
break;

case 2:
xwaddstr(win1,"\n*** 這桌沒玩三條的唷!",0);
return;

default:
xwaddstr(win1,"\n*** 你出的牌不符合大老二的規則唷!",0);
return;
}
if (first_hand) {
if (mycard[a[argc-1]-1].point!=3 || mycard[a[argc-1]-1].suit!='C') {
xwaddstr(win1,"\n*** 第一次出牌裡要含有梅花三(3C)!",0);
return;
}}
first_hand=0;
/* give out the cards now */
sprintf(tmp,"Gg%s\n%s\n%s\n%d#",o[0].nick,o[1].nick,o[2].nick,argc);
new_write(sockfd,tmp,strlen(tmp));
for(n=0;n<argc;n++) {
sprintf(tmp,"%c%c",convert3(mycard[a[n]-1].point),mycard[a[n]-1].suit);
new_write(sockfd,tmp,strlen(tmp));
mycard[a[n]-1].point=(int)NULL;
mycard[a[n]-1].suit=(char)NULL;
}
sort_cards(self.cardleft);
card_go_out = 1;
}

arrow_give_card(c)
chtype c;
{
int r,i;
r=0;
mvwaddch(winB,4,2+(cardposx*4),' ');
if (c==KEY_RIGHT || c==6) {cardposx++; if (cardposx==self.cardleft) cardposx=0; r=1;}
if (c==KEY_LEFT || c==2) {cardposx--; if (cardposx<0) cardposx=self.cardleft-1; r=1;}
if ((char)c==32) {toggle_queue(); r=1;}
i=self.cardleft+1;
if (((char)c>64 && (char)c<64+i) || ((char)c>96 && (char)c<i+96)) {alpha_toggle_queue((char)c); r=1;}
if ((char)c=='\n') {bzero(buffer,sizeof(buffer)); for(i=0;i<qlen;i++) {sprintf(tmp,"%d ",queue[i]); strcpy(buffer,strcat(buffer,tmp));} if (!qlen) strcpy(buffer,"0"); give_card(buffer); r=1;}
mvwaddch(winB,4,2+(cardposx*4),'*');
wrefresh(winB);
if (!r) {/*xwaddstr(win1,"\n*** 錯誤! 在出牌模式下使用未定義的按鍵,請按 TAB 鍵可切換回交談模式.",0); */if (beeper) beep();}
}

alpha_toggle_queue(c)
char c;
{
int i;
char a;
a=toupper(c);
cardposx=a-65;
toggle_queue();
}

toggle_queue()
{
int i,j;
if ((i=seekinqueue(cardposx+1))!=-1) {
mvwaddch(winB,4,3+(cardposx*4),65+cardposx);
j=qlen-1;
if (i!=j) queue[i]=queue[j];
qlen--;
return;
}
if (qlen==5) {
xwaddstr(win1,"\n*** 一次不能給超過 5 張喔!",0);
return;
}
queue[qlen]=cardposx+1;
if (term_type==2)
wattron(winB,A_REVERSE);
else
wattron(winB,COLOR_PAIR(5));
wattron(winB,A_BOLD);
mvwaddch(winB,4,3+(cardposx*4),65+cardposx);
if (term_type==2)
wattroff(winB,A_REVERSE);
else
wattroff(winB,A_BOLD);
wattroff(winB,COLOR_PAIR(5));
qlen++;
}

seekinqueue(pos) /* pos = 1 ~ 13 */
int pos;
{
int i;
for(i=0;i<qlen;i++)
if (queue[i]==pos) return(i);
return(-1);
}

