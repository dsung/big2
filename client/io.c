#include "big2.h"

void
backone()
{
int yy,xx;
getyx(winu,yy,xx);
wmove(winu,yy,xx-1);
waddch(winu,' ');
wrefresh(winu);
wmove(winu,yy,xx-1);
}

void
getdata1(Y,X,start,count,what,msg,show_char,clean,break_ok)
int Y,X,start,count,show_char,clean,break_ok;
char *what,*msg;
{
int yy,xx,i;
int get;
mvwprintw(winu,0,0,"%-80s",msg);
wrefresh(winu);
for(i=0;i<count;i++) {
get_the_ok:
wmove(winu,0,start+i);
get=wgetch(winu);
if (get=='\n') {if (!i) goto get_the_ok; what[i]='\0'; break;}
if (break_ok && is_break(get)) {writech('Q'); nclient_exit(0);}
if (is_back(get) && i) {backone(); i-=2; continue;}
if (!is_d_a(get)) goto get_the_ok;
what[i]=get;
if (show_char) waddch(winu,get); else waddch(winu,'*');
wrefresh(winu);
}
if (clean) {wmove(winu,0,0); wclrtoeol(winu); wrefresh(winu);}
}

