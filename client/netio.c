#include "big2.h"

int sockfd;
struct sockaddr_in srv_addr;

void
nwrite(s)
char *s;
{
write(sockfd,s,strlen(s));
}

char
readch()
{
char c;
read(sockfd,&c,1);
return(c);
}

int
get_srvmsg(s,stop)
char *s,stop;
{
int n;
for(n=0;;n++) {
s[n]=readch();
if (s[n]==stop) break; }
s[n]='\0';
return(n);
}

void
connect_server()
{
sockfd=socket(PF_INET,SOCK_STREAM,0);
bzero((char *)&srv_addr,sizeof(srv_addr));
srv_addr.sin_family=AF_INET;
srv_addr.sin_addr.s_addr=inet_addr(SERV_ADDR);
srv_addr.sin_port=htons(SERV_PORT);
waddstr(win1,"*** 嘗試連往網路大老二 Server..");
wrefresh(win1);
if (connect(sockfd,(struct sockaddr *)&srv_addr,sizeof(srv_addr))<0) {
#ifdef AIX
wprintw(win1,"失敗.\n*** 系統錯誤訊息 -> [Connection Refused]\n*** 很抱歉!! 網路大老二 Server 出了點狀況, 請稍後再試.");
#else
wprintw(win1,"失敗.\n*** 系統錯誤訊息 -> [%s]\n*** 很抱歉!! 網路大老二 Server 出了點狀況, 請稍後再試.",sys_errlist[errno]);
#endif
nclient_exit(1);
}
waddstr(win1,"成功\.");
condition(1);
sprintf(tmp,"%c%c%s\n%s\n%s\n",2,1,self.uname,self.domain,CLIENT_VERSION);
nwrite(tmp);
get_srvmsg(buffer,3);
waddstr(win1,"\n*** Server message:\n");
wprintw(win1,buffer);
wrefresh(win1);
switch(readch()) {
case 1: 
wprintw(win1,"\n*** 你所使用的版本(Ver %s Beta)是舊的, 請立即更新 client 的版本.",CLIENT_VERSION);
nclient_exit(0);
break;
case 2:
wprintw(win1,"\n*** 你所連來的 IP 位址 -> [%s] 已被系統管裡者列入禁止通行名單中.",ipaddress);
nclient_exit(0);
break;
case 3:
waddstr(win1,"\n*** 目前線上總人數已達上限, 請稍後再試, 謝謝.");
nclient_exit(0);
break;
case 10:
waddstr(win1,"\n*** (保留功\能) 大老二伺服器將你強制結束.");
nclient_exit(0);
break;
case 11:
break;
default:
nclient_exit(0);
}
}

