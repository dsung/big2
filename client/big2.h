#include "../conf/common.h"

#define SERV_ADDR "127.0.0.1"
#define SCROLL_BACK_LINES 10

#include <curses.h>
#include <stdlib.h>
#include <sys/errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define YEA (1)
#define NA (0)

#ifdef AIX
#include <time.h>
#include <sys/select.h>
#else
#include <sys/time.h>
#endif

#include <unistd.h>
#include <pwd.h>
#include <signal.h>
#include <netdb.h>
#include <sys/utsname.h>
#include <string.h>
#include <sys/param.h>

extern int system();
extern void connect_server();
extern int sockfd;
#ifdef SUNOS
extern int errno;
extern char **sys_errlist;
#endif
extern char tmp[1024],buffer[3000];
extern char *username;
extern char *ipaddress;
extern char *domainname;
extern char readch();
extern int isprint2();
extern int get_srvmsg();
extern void login();
extern char logintime[50];
extern int total_online,total_register,serial;
extern int away_flag;
extern int color_mode;
extern int card_go_out;

extern void condition();

struct player {
char nick[11];
char password[11];
int desk,on_game;
int cardleft;
int level; /* the 桌長 = 30 ; 系統管理員 = 100 */
int fd;
int money,win,lose,login;
int serial; /* 在 file 裡頭的序號 */
char uname[40];
char domain[40];
char ip[40];
char kernel[40];
char away_note[156];
int away_flag;
}; 

struct card {
int point;
char suit;
int flag;
};

/* start */

#define ESC		0x1b	/* Esc char */
#define	BACK		0x7f
#define	CTRLC		0x03
#define	CTRLD		0x04
#define	CTRLH		0x08
#define	CTRLL		0x0c

#ifndef	MIN
#  define MIN(a,b) ((a<b)?a:b)
#endif

#ifndef	MAX
#  define MAX(a,b) ((a>b)?a:b)
#endif

/* end */
/* end */

extern int deskman,badsrvinfo,nn,term_type;
extern int last_n,pause_position,last_n_fake,pass_time,bet_times;
extern int tabkey_mode,beeper,arrow1,quitcheck,threecard,cardposx,queue[6],qlen,first_hand,idlemin,key_press,china_sea,screen_wash;
extern int last_min,last_total_online,last_deskman,last_level,last_desk; /* to decrease the frequency of condition_win() refresh() */
extern char tmp2[1024],tmp3[1024],tmp4[1024],tmp5[1024],buffer2[],buffer3[],lastcmd[SCROLL_BACK_LINES][256];
extern char lastmsg[256],lastgivenick[11],away_note[256];

extern struct player self,o[3],g[4],finger;
extern struct card mycard[13],last[6];

extern char readch();
extern char style();
extern char convert3();
extern char *convert4();
extern char *convert5();
extern char *on_game_check();

extern WINDOW *win0,*win1,*winc,*winu,*win2;
extern WINDOW *winA,*winB,*winS,*wins,*winC;

