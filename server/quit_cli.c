#include "big2d.h"

void
kill_cli(fd,why)
int fd,why;
{
FD_CLR(fd,&rfds);
if (close(fd)==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d kill_cli close fails %s # %s",fd,sys_errlist[errno],ctime(&now));
fflush(error_fp);
}
time(&now);
fseek(error_fp,0,SEEK_END);
switch(why) {
case 0:
fprintf(error_fp,"* fd=%03d (kill_cli) (unregister_quit_cli)     # %s",fd,ctime(&now));
break;
case 1:
fprintf(error_fp,"* fd=%03d (kill_cli) (too old client version)  # %s",fd,ctime(&now));
break;
case 2:
fprintf(error_fp,"* fd=%03d (kill_cli) (ip locked)               # %s",fd,ctime(&now));
break;
case 3:
fprintf(error_fp,"* fd=%03d (kill_cli) (player full;now -> %03d)  # %s",fd,total_online,ctime(&now));
break;
case 4:
fprintf(error_fp,"* fd=%03d (kill_cli) (passwd 3 times error)    # %s",fd,ctime(&now));
break;
case 5:
fprintf(error_fp,"* fd=%03d (kill_cli) (break while login)       # %s",fd,ctime(&now));
break;
case 10:
fprintf(error_fp,"* fd=%03d (kill_cli) (reversed_usage)          # %s",fd,ctime(&now));
break;
default:
fprintf(error_fp,"* fd=%03d (kill_cli)                           # %s",fd,ctime(&now));
break;
}
fflush(error_fp);
}

void
unregister_quit_cli(fd)
int fd;
{
char quit_info[80];
sprintf(quit_info,"使用尚未註冊的 client !! 強制離開.\n");
new_write(fd,quit_info,strlen(quit_info));
kill_cli(fd,0);
}

quit_cli2(fd)
int fd;
{
int i;
i=onlineseek(fd);
if (i==-1) {quit_cli(fd); longjmp(jmp_environment,jmp_val);}
if (online[i].error>=max_error_times) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d [%10s] max_error_time exceeded # %s",fd,online[i].nick,ctime(&now));
fflush(error_fp);
quit_cli(fd);
longjmp(jmp_environment,jmp_val);
}
online[i].error++;
}

quit_cli3(fd)
int fd;
{
int i=0;
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d quit_cli3 called # %s",fd,ctime(&now));
fflush(error_fp);
active[fd].bad=1;
if (!active[fd].close) {i=close(fd); active[fd].close=1;}
if (i==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d quit_cli3 close fails %s # %s",fd,sys_errlist[errno],ctime(&now));
fflush(error_fp);
}
}

quit_cli(fd)
int fd;
{
int i,j,dsk,m;
char c='Q';
if (write(fd,&c,1)<0) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d quit_cli write-ch blocked %s # %s",fd,sys_errlist[errno],ctime(&now));
fflush(error_fp);
}
FD_CLR(fd,&rfds);
if (!active[fd].close) m=close(fd);
if (m==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d quit_cli close fails %s # %s",fd,sys_errlist[errno],ctime(&now));
fflush(error_fp);
}
active[fd].flag=0;
if ((i=onlineseek(fd))!=-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d [%10s]                  quited # %s",fd,online[i].nick,ctime(&now));
fflush(error_fp);
if ((dsk=online[i].desk)!=-1) {
bzero(buffer,sizeof(buffer));
strcpy(buffer,"Go");
if (online[i].on_game) {
for(j=0;j<total_online;j++) 
if (online[j].desk==dsk && j!=i) 
{new_write(online[j].fd,buffer,strlen(buffer)); online[j].on_game=0; online[j].cardleft=0;}            }
sprintf(buffer,"Bq%s\n",online[i].nick);
for(j=0;j<total_online;j++) 
if (online[j].desk==dsk && j!=i) 
new_write(online[j].fd,buffer,strlen(buffer));
                              }
/* online[i].desk if */
j=total_online-1;
total_online--;
if (i!=j) {
strcpy(online[i].nick,    online[j].nick);
strcpy(online[i].password,online[j].password);
strcpy(online[i].uname,   online[j].uname);
strcpy(online[i].domain,  online[j].domain);
strcpy(online[i].ip,      online[j].ip);
online[i].fd=             online[j].fd;
online[i].desk=           online[j].desk;
online[i].on_game=        online[j].on_game;
online[i].level=          online[j].level;
online[i].money=          online[j].money;
online[i].win=            online[j].win;
online[i].lose=           online[j].lose;
online[i].login=          online[j].login;
online[i].serial=         online[j].serial;
online[i].error=          online[j].error;
online[i].cardleft=       online[j].cardleft;
online[i].timeout =       online[j].timeout; }
}
}
