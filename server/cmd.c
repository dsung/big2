#include "big2d.h"

join_desk(fd)
int fd;
{
int i,j,k,l,dsk,m,n,a,b,c,d;
char tmp[30];
char who[80],where[80];
rcv_msg(fd,tmp,'#');
rcv_msg(fd,who,'\n');
rcv_msg(fd,where,'\n');
dsk=atoi(tmp);
i=onlineseek(fd);
if (online[i].desk!=-1) return;
sprintf(buffer,"Bj%s#%s\n%s\n",online[i].nick,who,where); 
for(l=j=0;j<total_online;j++)  /* for the desk's people where fd wants to join */
if (online[j].desk==dsk) {new_write(online[j].fd,buffer,strlen(buffer)); l++;}
/* for fd itself */
if (!l) {strcpy(desk_topic[dsk],DEFAULT_DESK_TOPIC); deskbet[dsk]=0; online[i].level=30;}
online[i].desk=dsk;
bzero(users,sizeof(users));
for(i=j=0;j<total_online;j++) 
if (online[j].desk==dsk) {
i++;
if (online[j].level==30) 
sprintf(tmp,"@%s ",online[j].nick);
else 
{
if (online[j].level==100)
sprintf(tmp,"#%s ",online[j].nick);
else
sprintf(tmp,"%s ",online[j].nick);
}
strcpy(users,strcat(users,tmp));
}
sprintf(tmp,"Sj%d#%d#%d#",dsk,i,deskbet[dsk]);
new_write(fd,tmp,strlen(tmp));
new_write(fd,desk_topic[dsk],strlen(desk_topic[dsk]));
writech(fd,'\n');
new_write(fd,users,strlen(users));
writech(fd,'\n');
/* for game part */
for(m=n=0;n<total_online;n++) if (online[n].on_game && online[n].desk==dsk) m++;
if (m) {
a=onlineseeknick2(desk_player_nick[dsk][0]);
b=onlineseeknick2(desk_player_nick[dsk][1]);
c=onlineseeknick2(desk_player_nick[dsk][2]);
d=onlineseeknick2(desk_player_nick[dsk][3]);
sprintf(users,"GiJ%s\n%s\n%s\n%s\n%d#%d#%d#%d#",desk_player_nick[dsk][0],desk_player_nick[dsk][1],desk_player_nick[dsk][2],desk_player_nick[dsk][3],online[a].money,online[b].money,online[c].money,online[d].money);
new_write(fd,users,strlen(users));
sprintf(users,"Gs%d#%d#%d#%d#%s\n",online[a].cardleft,online[b].cardleft,online[c].cardleft,online[d].cardleft,desk_turn[dsk]);
new_write(fd,users,strlen(users));
}
}

part_desk(fd)
int fd;
{
int i,j,k,l;
i=onlineseek(fd);
if (online[i].desk==-1 || online[i].on_game) return;
sprintf(buffer,"Bp%s\n",online[i].nick);
for(j=0;j<total_online;j++) 
if (online[j].desk==online[i].desk && i!=j) 
new_write(online[j].fd,buffer,strlen(buffer));
writech(fd,'S'); writech(fd,'p');
for(l=k=0;k<total_online;k++) if (online[k].on_game && k!=i && online[k].desk==online[i].desk) l++;
if (l) writech(fd,'A'); else writech(fd,'B');
online[i].desk=-1;
online[i].on_game=0;
online[i].level=0;
online[i].cardleft=0;
}

action(fd)
int fd;
{
int i,j;
rcv_msg(fd,users,'\n');
i=onlineseek(fd);
sprintf(buffer,"Bb%s\n%s\n",online[i].nick,users); 
for(j=0;j<total_online;j++)  /* for the desk's people where fd wants to join */
if (online[j].desk==online[i].desk && j!=i) new_write(online[j].fd,buffer,strlen(buffer));
/* for fd itself */
sprintf(buffer,"Sb%s\n",users);
new_write(fd,buffer,strlen(buffer));
}

send_all(fd)
int fd;
{
int i;
rcv_msg(fd,users,'\n');
sprintf(buffer,"Bc%s\n",users);
for(i=0;i<total_online;i++) new_write(online[i].fd,buffer,strlen(buffer));
}

say_words(fd)
int fd;
{
int i,j;
rcv_msg(fd,users,'\n');
i=onlineseek(fd);
if (online[i].desk==-1) return;
sprintf(buffer,"Bs%s\n%s\n",online[i].nick,users); 
for(j=0;j<total_online;j++) 
if (online[j].desk==online[i].desk && j!=i) new_write(online[j].fd,buffer,strlen(buffer));
sprintf(buffer,"Ss%s\n",users);
new_write(fd,buffer,strlen(buffer));
}

max_online(fd)
int fd;
{
char tmp[20];
rcv_msg(fd,tmp,'\n');
onlinemax=atoi(tmp);
sprintf(users,"An%s\n",tmp);
new_write(fd,users,strlen(users));
}

money_change(fd)
int fd;
{
int i,j,money;
char tmp[20];
rcv_msg(fd,tmp,'\n');
rcv_msg(fd,users,'#');
money=atoi(users);
if ((i=onlineseeknick(tmp))==-1) {
if ((j=databaseseeknick(tmp))==-1) {
sprintf(users,"Am%s\nN",tmp);
new_write(fd,users,strlen(users));
return;
}
database[j].money=money;
fseek(fp,(long)j*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[j].nick,database[j].password,database[j].uname,database[j].domain,database[j].ip,database[j].money,database[j].win,database[j].lose,database[j].login);
fflush(fp);
sprintf(buffer,"Am%s\nY%s#",tmp,users);
new_write(fd,buffer,strlen(buffer));
return;
}
j=onlineseek(i);
if (online[j].on_game) {
sprintf(users,"Am%s\nG",tmp);
new_write(fd,users,strlen(users));
return;
}
online[j].money=money;
j=databaseseeknick(tmp);
database[j].money=money;
fseek(fp,(long)j*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[j].nick,database[j].password,database[j].uname,database[j].domain,database[j].ip,database[j].money,database[j].win,database[j].lose,database[j].login);
fflush(fp);
sprintf(buffer,"Am%s\nY%s#",tmp,users);
new_write(fd,buffer,strlen(buffer));
sprintf(buffer,"Bd%s#",users);
new_write(i,buffer,strlen(buffer));
}

send_msg(fd)
int fd;
{
int i;
char tmp[20];
rcv_msg(fd,tmp,'\n');
rcv_msg(fd,users,'\n');
if ((i=onlineseeknick(tmp))==-1) {
sprintf(users,"BmN%s\n",tmp);
new_write(fd,users,strlen(users));
return;
}
sprintf(buffer,"BmY%s\n%s\n",tmp,users);
new_write(fd,buffer,strlen(buffer));
sprintf(buffer,"BmM%s\n%s\n",online[onlineseek(fd)].nick,users);
new_write(i,buffer,strlen(buffer));
}

invite(fd)
int fd;
{
int i,j;
char tmp[20];
rcv_msg(fd,tmp,'\n');
j=onlineseek(fd);
if (online[j].desk==-1) return;
if ((i=onlineseeknick(tmp))==-1) {
sprintf(users,"BiN%s\n",tmp);
new_write(fd,users,strlen(users));
return;
}
sprintf(users,"BiY%s\n",tmp);
new_write(fd,users,strlen(users));
sprintf(users,"BiI%s\n%02d#",online[j].nick,online[j].desk);
new_write(i,users,strlen(users));
}

finger(fd)
int fd;
{
int i,j;
char tmp[20];
rcv_msg(fd,tmp,'\n');
if ((j=databaseseeknick(tmp))==-1) {
sprintf(users,"Bf%s\nN",tmp);
new_write(fd,users,strlen(users));
return;
}
if ((i=onlineseeknick(tmp))==-1) {
sprintf(users,"Bf%s\nO%s\n%s\n%s\n%d#%d#%d#%d#",tmp,database[j].uname,database[j].domain,database[j].ip,database[j].money,database[j].win,database[j].lose,database[j].login);
new_write(fd,users,strlen(users));
return;
}
sprintf(users,"Bf%s\nA",online[onlineseek(fd)].nick);
new_write(i,users,strlen(users));
}

finger_reply(fd)
int fd;
{
int i,away_mode;
char tmp[20],who[80],domain[80],ip[80],desk[5],on_game_check[10],kernel[100],money[20],win[10],lose[10],login[10],logintime[50],idlemin[10],away_note[356];
rcv_msg(fd,tmp,'\n'); /* tmp is the name of the asker */
rcv_msg(fd,who,'\n');
rcv_msg(fd,domain,'\n');
rcv_msg(fd,ip,'\n');
rcv_msg(fd,desk,'#');
rcv_msg(fd,on_game_check,'\n');
rcv_msg(fd,kernel,'\n');
rcv_msg(fd,logintime,'\n');
rcv_msg(fd,idlemin,'#');
rcv_msg(fd,money,'#');
rcv_msg(fd,win,'#');
rcv_msg(fd,lose,'#');
rcv_msg(fd,login,'#');
if (readch(fd)=='1') {rcv_msg(fd,away_note,'\n'); away_mode=1;} else away_mode = 0;
if (away_mode)
sprintf(users,"Bf%s\nY%s\n%s\n%s\n%s#%s\n%s\n%s#%s\n%s#%s#%s#%s#1%s\n",online[onlineseek(fd)].nick,who,domain,ip,desk,on_game_check,kernel,idlemin,logintime,money,win,lose,login,away_note);
else
sprintf(users,"Bf%s\nY%s\n%s\n%s\n%s#%s\n%s\n%s#%s\n%s#%s#%s#%s#0",online[onlineseek(fd)].nick,who,domain,ip,desk,on_game_check,kernel,idlemin,logintime,money,win,lose,login);
if ((i=onlineseeknick(tmp))==-1) return;
new_write(i,users,strlen(users));
}

admin(fd)
int fd;
{
int i;
rcv_msg(fd,buffer,'\n');
i=onlineseek(fd);
if (!strcmp(buffer,ADMIN_PASSWORD)) {
sprintf(buffer,"BaY");
new_write(fd,buffer,strlen(buffer));
online[i].level=100;
return;
}
sprintf(buffer,"BaN");
new_write(fd,buffer,strlen(buffer));
}

shutdown_server(fd)
int fd;
{
int i,shutdown_secs;
rcv_msg(fd,users,'\n');
shutdown_secs=atoi(users);
sprintf(users,"Be%d\n",shutdown_secs);
for(i=0;i<total_online;i++) 
write(online[i].fd,users,strlen(users));
if (shutdown_secs==-1) {
for(i=0;i<total_online;i++) 
writech(online[i].fd,'Q');
exit(0);
}
}

exit_admin(fd)
int fd;
{
online[onlineseek(fd)].level=0;
writech(fd,'A');
writech(fd,'e');
}

kill_user(fd)
int fd;
{
int i;
char tmp[20];
rcv_msg(fd,tmp,'\n');
if ((i=onlineseeknick(tmp))==-1) {
sprintf(users,"Ak%s\nN",tmp);
new_write(fd,users,strlen(users));
return;
}
sprintf(users,"Ak%s\nY",tmp);
new_write(fd,users,strlen(users));
quit_cli(i);
}

kill_user2(fd)
int fd;
{
int i;
char tmp[20];
rcv_msg(fd,tmp,'\n');
if ((i=onlineseeknick(tmp))==-1) return;
sprintf(users,"Bk");
new_write(i,users,strlen(users));
}

change_bet(fd)
int fd;
{
int i,j,k;
char tmp[10];
rcv_msg(fd,tmp,'\n');
i=onlineseek(fd);
if (online[i].desk==-1) return;
/* make sure player on_game in his desk */
for(k=j=0;j<total_online;j++) if (online[j].desk==online[i].desk && i!=j && online[j].on_game) k++;
if (!k) return;
deskbet[online[i].desk]=atoi(tmp);
sprintf(users,"Bv%s\n%s#",online[i].nick,tmp);
for(j=0;j<total_online;j++) if (online[j].desk==online[i].desk && i!=j) new_write(online[j].fd,users,strlen(users));
sprintf(users,"Sv%s#",tmp);
new_write(fd,users,strlen(users));
}

oper(fd)
int fd;
{
int i,j,k;
char tmp[20];
rcv_msg(fd,tmp,'\n');
if ((i=onlineseeknick(tmp))==-1) {
sprintf(users,"Bo%s\nN",tmp);
new_write(fd,users,strlen(users));
return;
}
j=onlineseek(i);
i=onlineseek(fd);
if (online[j].desk!=online[i].desk) {
sprintf(users,"Bo%s\nD",tmp);
new_write(fd,users,strlen(users));
return;
}
sprintf(users,"Bo%s\nO",tmp);
new_write(fd,users,strlen(users));
sprintf(users,"Bo%s\no",online[i].nick);
new_write(online[j].fd,users,strlen(users));
sprintf(users,"Bo%s\nA%s\n",online[i].nick,tmp);
online[j].level=30;
for(k=0;k<total_online;k++) if (online[k].desk==online[i].desk && k!=i && k!=j) new_write(online[k].fd,users,strlen(users));
}

game_init(fd)
int fd;
{
int i,j,Fd[3],ser[3],k,threecard,m,l,rand_seed;
char o[3][11];
struct card newcards[52];
threecard=0;
if (readch(fd)=='3') threecard=1; /* ���T�� */
for(i=0;i<3;i++) rcv_msg(fd,o[i],'\n');
rcv_msg(fd,users,'\n');
rand_seed=atoi(users);
j=onlineseek(fd);
if (online[j].on_game) return;
for(i=0;i<3;i++) if ((Fd[i]=onlineseeknick(o[i]))==-1) {
sprintf(users,"GiN%s\n",o[i]);
new_write(fd,users,strlen(users));
return;
}
for(i=0;i<3;i++) {
ser[i]=onlineseek(Fd[i]);
if (online[ser[i]].desk!=online[j].desk) {
sprintf(users,"GiD%s\n",o[i]);
new_write(fd,users,strlen(users));
return; }
if (online[ser[i]].on_game) {
sprintf(users,"GiO%s\n",o[i]);
new_write(fd,users,strlen(users));
return; }
}
for(i=0;i<total_online;i++)
if (online[i].on_game && online[i].desk==online[j].desk) {
sprintf(users,"GiA");
new_write(fd,users,strlen(users));
return; }
/* so far the four people are in the same desk, no ppl playing here */
sprintf(users,"GiG%d#%s\n%s\n%s\n2#%d#%d#%d#",threecard,o[1],o[2],online[j].nick,online[ser[1]].money,online[ser[2]].money,online[j].money);
new_write(Fd[0],users,strlen(users));
online[ser[0]].on_game=2;
sprintf(users,"GiG%d#%s\n%s\n%s\n3#%d#%d#%d#",threecard,o[2],online[j].nick,o[0],online[ser[2]].money,online[j].money,online[ser[0]].money);
new_write(Fd[1],users,strlen(users));
online[ser[1]].on_game=3;
sprintf(users,"GiG%d#%s\n%s\n%s\n4#%d#%d#%d#",threecard,online[j].nick,o[0],o[1],online[j].money,online[ser[0]].money,online[ser[1]].money);
new_write(Fd[2],users,strlen(users));
online[ser[2]].on_game=4;
sprintf(users,"GiH%d#%s\n%s\n%s\n%d#%d#%d#",threecard,o[0],o[1],o[2],online[ser[0]].money,online[ser[1]].money,online[ser[2]].money);
new_write(fd,users,strlen(users));
online[j].on_game=1;
/* give this message to the rest */
sprintf(users,"GiM%s\n%s\n%s\n%s\n%d#%d#%d#%d#",online[j].nick,o[0],o[1],o[2],online[j].money,online[ser[0]].money,online[ser[1]].money,online[ser[2]].money);
for (k=0;k<total_online;k++) if(online[k].desk==online[j].desk && !online[k].on_game) new_write(online[k].fd,users,strlen(users));
/* special .. */
strcpy(desk_player_nick[online[j].desk][0],online[j].nick);
strcpy(desk_player_nick[online[j].desk][1],o[0]);
strcpy(desk_player_nick[online[j].desk][2],o[1]);
strcpy(desk_player_nick[online[j].desk][3],o[2]);
/* shuffle and  give the cards */
shuffle(newcards,rand_seed);
give_cards(fd,0,newcards,13);
give_cards(Fd[0],0,newcards+13,13);
give_cards(Fd[1],0,newcards+26,13);
give_cards(Fd[2],0,newcards+39,13);
online[j].cardleft=13;
online[ser[0]].cardleft=13;
online[ser[1]].cardleft=13;
online[ser[2]].cardleft=13;
for(l=m=0;m<52;m++) if (newcards[m].point==3 && newcards[m].suit=='C') {l=m; break;}
if (l<13 && l>-1) {strcpy(desk_turn[online[j].desk],online[j].nick); writech(fd,'1'); writech(Fd[0],'2'); writech(Fd[1],'3'); writech(Fd[2],'4');}
if (l<26 && l>12) {strcpy(desk_turn[online[j].desk],online[ser[0]].nick); writech(fd,'4'); writech(Fd[0],'1'); writech(Fd[1],'2'); writech(Fd[2],'3');}
if (l<39 && l>25) {strcpy(desk_turn[online[j].desk],online[ser[1]].nick); writech(fd,'3'); writech(Fd[0],'4'); writech(Fd[1],'1'); writech(Fd[2],'2');}
if (l<52 && l>38) {strcpy(desk_turn[online[j].desk],online[ser[2]].nick); writech(fd,'2'); writech(Fd[0],'3'); writech(Fd[1],'4'); writech(Fd[2],'1');}
/* give to the rest */ 
for (k=0;k<total_online;k++) if (online[k].desk==online[j].desk && !online[k].on_game) give_cards(online[k].fd,3,newcards,l);
/* special */
bzero((char *)desklast[online[j].desk],sizeof(desklast[online[j].desk]));
}

char style(n)
int n;
{
if (0 <= n && n <= 12) return('S');
if (13 <= n && n <= 25) return('H');
if (26 <= n && n <= 38) return('D');
if (39 <= n && n <= 51) return('C');
}

shuffle(a,m)
int m;
struct card *a;
{
int i,p;
for(i=0;i<52;i++) a[i].flag=0;
for(i=0;i<52;i++) {
do { p=(int)(52.0*rand()/RAND_MAX);
} while(a[p].flag==1);
a[p].point=(54-i)%13;
a[p].suit=style(i);
a[p].flag=1;      }
}


char convert1(n)
int n;
{    
if (n==2) return('A');
if (n==1) return('B');
if (n==0) return('C');
if (n==12) return('D');
if (n==11) return('E');
if (n==10) return('F');
if (n==9) return('G');
if (n==8) return('H');
if (n==7) return('I');
if (n==6) return('J');
if (n==5) return('K');
if (n==4) return('L');
if (n==3) return('M');
}

convert3(c)
char c;
{
if (c=='A') return(2);
if (c=='B') return(1);
if (c=='C') return(0);
if (c=='D') return(12);
if (c=='E') return(11);
if (c=='F') return(10);
if (c=='G') return(9);
if (c=='H') return(8);
if (c=='I') return(7);
if (c=='J') return(6);
if (c=='K') return(5);
if (c=='L') return(4);
if (c=='M') return(3);
}

give_cards(fd,kind,begin,count)
int fd,kind,count;
struct card *begin;
{
int n;
char tmp[3];
if (!kind) {
sprintf(tmp,"Gg");
new_write(fd,tmp,strlen(tmp));
}
if (kind==1) {
sprintf(tmp,"Gh");
new_write(fd,tmp,strlen(tmp));
}
if (kind==2) {
sprintf(tmp,"Gj");
new_write(fd,tmp,strlen(tmp));
}
if (kind==3) {
sprintf(tmp,"Gr");
new_write(fd,tmp,strlen(tmp));
if (count>-1 && count<13) writech(fd,'0');
if (count>12 && count<26) writech(fd,'1');
if (count>25 && count<39) writech(fd,'2');
if (count>38 && count<52) writech(fd,'3');
return;
}
if (kind==4) {
sprintf(tmp,"Gy");
new_write(fd,tmp,strlen(tmp));
}
for(n=0;n<count;n++) {
writech(fd,convert1(begin[n].point));
writech(fd,begin[n].suit);
}
writech(fd,'\n');
}

get_cards(fd)
int fd;
{
int i,j,n,Fd[3],a,b,c,d,A,B,C,D,better;
int k,l;
char tmp[20];
rcv_msg(fd,tmp,'\n'); Fd[0]=onlineseeknick(tmp);
rcv_msg(fd,tmp,'\n'); Fd[1]=onlineseeknick(tmp);
rcv_msg(fd,tmp,'\n'); Fd[2]=onlineseeknick(tmp);
rcv_msg(fd,tmp,'#'); n=atoi(tmp);
/* ready to get n cards (n <=5 )*/
for(i=0;i<n;i++) {
last[i].point=convert3(readch(fd));
last[i].suit=readch(fd);
}
if (Fd[0]==-1 || Fd[1]==-1 || Fd[2]==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** get_cards encounter Fd = -1 # %s",ctime(&now));
fflush(error_fp);
return;
}
/* check if the right turn */
a=j=onlineseek(fd);
b=onlineseek(Fd[0]);
c=onlineseek(Fd[1]);
d=onlineseek(Fd[2]);
if (strcmp(online[j].nick,desk_turn[online[j].desk])) return;
if (!online[j].on_game || !online[b].on_game || !online[c].on_game || !online[d].on_game) return; 
if (online[j].desk == -1 || online[j].desk != online[b].desk || online[j].desk != online[c].desk || online[j].desk != online[d].desk || online[b].desk != online[c].desk || online[b].desk != online[d].desk || online[c].desk != online[d].desk) return;
for(i=0;i<n;i++) {
desklast[online[j].desk][i].point=last[i].point;
desklast[online[j].desk][i].suit=last[i].suit;
}
/* now give out to the four people */
give_cards(fd,2,desklast[online[j].desk],n);
give_cards(Fd[0],1,desklast[online[j].desk],n);
give_cards(Fd[1],1,desklast[online[j].desk],n);
give_cards(Fd[2],1,desklast[online[j].desk],n);
online[j].cardleft -= n;
/* special */
bzero(desk_turn[online[j].desk],sizeof(online[j].desk));
if (!strcmp(desk_player_nick[online[j].desk][0],online[j].nick)) strcpy(desk_turn[online[j].desk],desk_player_nick[online[j].desk][1]);
if (!strcmp(desk_player_nick[online[j].desk][1],online[j].nick)) strcpy(desk_turn[online[j].desk],desk_player_nick[online[j].desk][2]);
if (!strcmp(desk_player_nick[online[j].desk][2],online[j].nick)) strcpy(desk_turn[online[j].desk],desk_player_nick[online[j].desk][3]);
if (!strcmp(desk_player_nick[online[j].desk][3],online[j].nick)) strcpy(desk_turn[online[j].desk],desk_player_nick[online[j].desk][0]);
/* now give out to the rest who is not playing */
for(k=0;k<total_online;k++) if (online[k].desk==online[j].desk && !online[k].on_game) give_cards(online[k].fd,4,desklast[online[j].desk],n);
/* check the cardleft */
if (!online[j].cardleft) {
A=databaseseeknick(online[a].nick);
B=databaseseeknick(online[b].nick);
C=databaseseeknick(online[c].nick);
D=databaseseeknick(online[d].nick);
better=deskbet[online[a].desk];
online[a].money+=((online[b].cardleft+online[c].cardleft+online[d].cardleft)*better);
database[A].money=online[a].money;
online[b].money-=(online[b].cardleft*better);
if (online[b].money <= 0) online[b].money=500;
database[B].money=online[b].money;
online[c].money-=(online[c].cardleft*better);
if (online[c].money <= 0) online[c].money=500;
database[C].money=online[c].money;
online[d].money-=(online[d].cardleft*better);
if (online[d].money <= 0) online[d].money=500;
database[D].money=online[d].money;
online[a].win++;
database[A].win=online[a].win;
online[b].lose++;
database[B].lose=online[b].lose;
online[c].lose++;
database[C].lose=online[c].lose;
online[d].lose++;
database[D].lose=online[d].lose;
fseek(fp,(long)A*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[A].nick,database[A].password,database[A].uname,database[A].domain,database[A].ip,database[A].money,database[A].win,database[A].lose,database[A].login);
fseek(fp,(long)B*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[B].nick,database[B].password,database[B].uname,database[B].domain,database[B].ip,database[B].money,database[B].win,database[B].lose,database[B].login);
fseek(fp,(long)C*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[C].nick,database[C].password,database[C].uname,database[C].domain,database[C].ip,database[C].money,database[C].win,database[C].lose,database[C].login);
fseek(fp,(long)D*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[D].nick,database[D].password,database[D].uname,database[D].domain,database[D].ip,database[D].money,database[D].win,database[D].lose,database[D].login);
fflush(fp);
online[a].on_game=0;
online[b].on_game=0;
online[c].on_game=0;
online[d].on_game=0;
}
}

user_list(fd)
int fd;
{
int i;
char tmp[64];
char tmp2[20];
sprintf(tmp,"Bu%d#",total_online);
new_write(fd,tmp,strlen(tmp));
for (i=0;i<total_online;i++) {
sprintf(tmp2," %s",online[i].nick);
if (online[i].level==30) sprintf(tmp2,"@%s",online[i].nick);
if (online[i].level==100) sprintf(tmp2,"#%s",online[i].nick);
sprintf(tmp,"%s %d %d\n",tmp2,online[i].desk,online[i].on_game);
new_write(fd,tmp,strlen(tmp));
}
}

who_list(fd)
int fd;
{
int i,j,k;
char tmp[164];
char tmp2[20];
rcv_msg(fd,tmp,'#');
j=atoi(tmp);
for(k=i=0;i<total_online;i++)
if (online[i].desk==j) k++;
sprintf(tmp,"Bw%02d#%d#",j,k);
new_write(fd,tmp,strlen(tmp));
for(i=0;i<total_online;i++) 
if (online[i].desk==j)
{
sprintf(tmp2," %s",online[i].nick);
if (online[i].level==30) sprintf(tmp2,"@%s",online[i].nick);
if (online[i].level==100) sprintf(tmp2,"#%s",online[i].nick);
sprintf(tmp,"%s %d %d %d %d %d\n",tmp2,online[i].on_game,online[i].win,online[i].lose,online[i].money,online[i].login);
new_write(fd,tmp,strlen(tmp));
}
}

desk_list(fd)
int fd;
{
int i,j,desk[101];
char tmp[64];
bzero((char *)desk,sizeof(desk));
for(i=0;i<total_online;i++) desk[(online[i].desk)+1]++;
for(j=0,i=1;i<101;i++) if (desk[i]) j++;
sprintf(tmp,"Bl%d\n",j);
new_write(fd,tmp,strlen(tmp));
for (i=1;i<101;i++) if (desk[i]) {
sprintf(buffer,"%d %d %d\n%s\n",i-1,desk[i],deskbet[i-1],desk_topic[i-1]);
new_write(fd,buffer,strlen(buffer));
}
}

rank_list(fd)
int fd;
{
int k,l,m,top[RANK_NUMBER]; /* top has the database[i]'s i */
int rank_start,r,i,j,tmp_money,tmp_serial;
char tmp[10];
rcv_msg(fd,tmp,'\n');
/* rank_start=atoi(tmp); */
rank_start=1;
if ((rank_start<1 || rank_start>register_number-RANK_NUMBER+1 )&& register_number>=RANK_NUMBER) {
sprintf(users,"BrN%d\n",register_number-RANK_NUMBER+1);
new_write(fd,users,strlen(users));
return;
}
if (register_number<RANK_NUMBER) {rank_start=1; r=register_number;} else r=RANK_NUMBER;
for(i=0;i<register_number;i++) {money[i].money=database[i].money; money[i].serial=i;}
/* for(i=0;i<register_number;i++) */
for(i=0;i<21;i++)
for(j=i+1;j<register_number;j++)
if (money[i].money < money[j].money) {tmp_money=money[j].money; money[j].money=money[i].money; money[i].money=tmp_money; tmp_serial=money[j].serial; money[j].serial=money[i].serial; money[i].serial=tmp_serial;}
for(i=0;i<r;i++) top[i]=money[i+rank_start-1].serial;
sprintf(users,"BrY%d\n%d\n",r,rank_start);
new_write(fd,users,strlen(users));
for(i=0;i<r;i++) {
sprintf(users,"%s %d %d %d %d\n",database[top[i]].nick,database[top[i]].money,database[top[i]].win,database[top[i]].lose,database[top[i]].login);
new_write(fd,users,strlen(users));
}
}

set_topic(fd)
int fd;
{
int i,j;
i=online[(j=onlineseek(fd))].desk;
rcv_msg(fd,desk_topic[i],'\n');
sprintf(buffer,"Bt%s\n%s\n",online[j].nick,desk_topic[i]);
for(i=0;i<total_online;i++) 
if (online[i].desk==online[j].desk && i!=j) new_write(online[i].fd,buffer,strlen(buffer));
sprintf(buffer,"St%s\n",desk_topic[online[j].desk]);
new_write(fd,buffer,strlen(buffer));
}

change_passwd(fd)
int fd;
{
int i;
char tmp[20];
rcv_msg(fd,tmp,'\n');
i=onlineseek(fd);
strcpy(online[i].password,tmp);
if ((i=databaseseeknick(online[i].nick))==-1) return;
strcpy(database[i].password,tmp);
fseek(fp,(long)i*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[i].nick,database[i].password,database[i].uname,database[i].domain,database[i].ip,database[i].money,database[i].win,database[i].lose,database[i].login);
fflush(fp);
writech(fd,'B');
writech(fd,'n');
}

announce(fd)
int fd;
{
int i;
rcv_msg(fd,users,'\n');
i=atoi(users);
rcv_msg(fd,users,'\n');
strcpy(announce_post[i-1],users);
sprintf(buffer,"Ap%d\n%s\n",i,users);
new_write(fd,buffer,strlen(buffer));
}

delay_time_change(fd)
int fd;
{
char tmp[10];
rcv_msg(fd,tmp,'\n');
DELAY_SECOND=atoi(tmp);
sprintf(tmp,"Al%d\n",DELAY_SECOND);
new_write(fd,tmp,strlen(tmp));
}

error_max_change(fd)
int fd;
{
char tmp[10];
rcv_msg(fd,tmp,'\n');
max_error_times=atoi(tmp);
sprintf(tmp,"AM%d\n",max_error_times);
new_write(fd,tmp,strlen(tmp));
}

away_reply(fd)
int fd;
{
int i,j;
char tmp[20];
char t[356];
rcv_msg(fd,tmp,'\n');
rcv_msg(fd,t,'\n');
i=onlineseeknick(tmp);
if (i==-1) return;
j=onlineseek(fd);
sprintf(users,"Bh%s\n%s\n",online[j].nick,t);
new_write(i,users,strlen(users));
}
