#include "../conf/common.h"

#define MAX_DATABASE 10000
#define ADMIN_PASSWORD "123456"
#define DATABASE_FILE "/tmp/big2_database" 
#define ERRLOGFILE "/tmp/big2_errorlog"
#define READLOG "/tmp/big2_readlog"
#define WRITELOG "/tmp/big2_writelog"
#define SYSOP_NICK "SYSOP"

#ifdef RAND_MAX
#undef RAND_MAX
#define RAND_MAX 2147483647 
#endif

#define DEFAULT_DESK_TOPIC ""
#define RANK_NUMBER 20
#define MAX_ERROR_READ_RANGE 512
#define ABS_ONLINE_LIMIT 240

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#ifdef AIX
#include <sys/select.h>
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <setjmp.h>
#include <sys/errno.h>
#include <sys/stat.h>


struct player {
char nick[11];
char password[11];
int desk,on_game;
int cardleft;
int level; /* the 桌長 = 30 ; 系統管理員 = 100 */
int fd;
int money,win,lose,login;
int serial; /* 在 file 裡頭的序號 */
int error;
char uname[40];
char domain[40];
char ip[40];
int timeout;
};


struct card {
int point;
char suit;
int flag;
};

struct rank {
int serial;
int money;
};

struct client {
int fd;
int flag; /* flag=0 means unregiststerd client */
int bad;
int close;
};

extern total_online;
extern int errno;
// extern char *sys_errlist[];
extern time_t now;
extern FILE *fp,*error_fp,*write_fp,*read_fp;
extern int onlinemax,register_number,serial,max_error_times;
extern int read_log_toggle,write_log_toggle;
extern char users[8192],buffer[8192];
extern fd_set rfds;
extern int DELAY_SECOND;
extern int jmp_val,jmp_flag;
extern jmp_buf jmp_environment;
extern int deskbet[100];
extern int sockfd,last_seed;

extern struct client active[ABS_ONLINE_LIMIT];
extern struct timeval timeout;
extern struct card last[6];
extern struct rank money[MAX_DATABASE];

extern char announce_post[3][356];
extern char desk_topic[100][356],desk_turn[100][21];
extern char desk_player_nick[100][4][21];
extern struct card desklast[100][6];
extern struct player online[];
extern struct player database[];

