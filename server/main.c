#include "big2d.h"

#include <time.h>

FILE *fp,*error_fp,*read_fp,*write_fp;
fd_set rfds;

int sockfd;
struct sockaddr_in srv_addr;
time_t now;
struct timeval timeout;
int jmp_val,jmp_flag;
jmp_buf jmp_environment;

struct player online[ABS_ONLINE_LIMIT],database[MAX_DATABASE];
struct card last[6],desklast[100][6];
struct rank money[MAX_DATABASE];
struct client active[ABS_ONLINE_LIMIT];

int total_online,register_number,serial,deskbet[100],write_log_toggle,read_log_toggle,last_seed,onlinemax,DELAY_SECOND,max_error_times;
char users[8192],buffer[8192];

char desk_topic[100][356];
char desk_player_nick[100][4][21];
char desk_turn[100][21];
char announce_post[3][356];

int ping_min;

main_server_loop(n)
int n;
{
int len,nfds,fd,newsockfd;
struct sockaddr_in new_addr;
int len3;
char totalman[10];
fd_set afds;
if (n==1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* (Initial) Main_server_loop called at %s",ctime(&now));
fflush(error_fp);
jmp_val=setjmp(jmp_environment);
if (!jmp_flag) return;}
for(;;) {
bcopy((char *)&rfds,(char *)&afds,sizeof(afds));
nfds=getdtablesize();
for(fd=0;fd<nfds;fd++)
if (fd!=sockfd && FD_ISSET(fd,&afds) && active[fd].bad) quit_cli(fd);
time(&now);
if (ping_min!=localtime(&now)->tm_min) {
ping_min=localtime(&now)->tm_min;
/* clean all timeout=1 */
for(fd=0;fd<total_online;fd++) if (online[fd].timeout) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d <%s> <ping timeout> # %s",online[fd].fd,online[fd].nick,ctime(&now));
fflush(error_fp);
quit_cli(online[fd].fd);
}
/* set timeout = 0 -> timeout = 1 -> finger ask them */
sprintf(users,"Bg");
for(fd=0;fd<total_online;fd++) if (!online[fd].timeout) {
online[fd].timeout=1;
new_write(online[fd].fd,users,strlen(users));
}
}
timeout.tv_sec=1;
timeout.tv_usec=0;
select(nfds,&afds,(fd_set *)0,(fd_set *)0,(struct timeval *)&timeout);
/* data emerges at sockfd */
if (FD_ISSET(sockfd,&afds)) {
len=sizeof(srv_addr);
if ((newsockfd=accept(sockfd,(struct sockaddr *)&srv_addr,&len))==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* accpet = -1 %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
longjmp(jmp_environment,jmp_val); }
if (onlineseek(newsockfd)!=-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d accpeted a already online ? # %s",newsockfd,ctime(&now));
fflush(error_fp);
while (onlineseek(newsockfd)!=-1) quit_cli(newsockfd);
continue; }
fcntl(newsockfd,F_SETFL,O_NDELAY);
FD_SET(newsockfd,&rfds);
active[newsockfd].flag = 0;
active[newsockfd].bad = 0;
active[newsockfd].close = 0;
len3=sizeof(new_addr);
if (getpeername(newsockfd,(struct sockaddr *)&new_addr,&len3)<0) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d getpeername fails -> [%s] accepted # %s",newsockfd,sys_errlist[errno],ctime(&now));
fflush(error_fp); }
else
{
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d From -> [%15s]   accepted # %s",newsockfd,inet_ntoa(new_addr.sin_addr),ctime(&now));
fflush(error_fp); }
}
/* check the rest */
for(fd=0;fd<nfds;fd++) if (fd!=sockfd && FD_ISSET(fd,&afds)) cli_service(fd);
for(fd=0;fd<nfds;fd++) if (fd!=sockfd && FD_ISSET(fd,&afds) && active[fd].bad) quit_cli(fd);
sprintf(totalman,"BT%d@",total_online);
for(fd=0;fd<total_online;fd++)
new_write(online[fd].fd,totalman,strlen(totalman));
}
}

main()
{
int on=1,ttyfd,rcvbuf=10240,len1=sizeof(rcvbuf),sndbuf=10240,len2=sizeof(sndbuf);
struct rlimit fd_limit;
struct linger lin;
/* cannot implement RLIMIT_NOFILE in AIX - search for solution */
getrlimit(RLIMIT_NOFILE,&fd_limit);
fd_limit.rlim_cur=fd_limit.rlim_max;
setrlimit(RLIMIT_NOFILE,&fd_limit);
/**/
fp=fopen(DATABASE_FILE,"a+r+");
error_fp=fopen(ERRLOGFILE,"a+r+");
read_fp=fopen(READLOG,"a+r+");
write_fp=fopen(WRITELOG,"a+r+");
sockfd=socket(PF_INET,SOCK_STREAM,0);
active[sockfd].bad = 0;
active[sockfd].close = 0;
active[sockfd].flag = 1;
bzero((char *)(&srv_addr),sizeof(srv_addr));
srv_addr.sin_family=AF_INET;
srv_addr.sin_addr.s_addr=htonl(INADDR_ANY);
srv_addr.sin_port=htons(SERV_PORT);
setsockopt(sockfd,SOL_SOCKET,SO_DEBUG,&on,sizeof(on));
setsockopt(sockfd,SOL_SOCKET,SO_KEEPALIVE,&on,sizeof(on));
setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
setsockopt(sockfd,SOL_SOCKET,SO_RCVBUF,&rcvbuf,len1);
setsockopt(sockfd,SOL_SOCKET,SO_SNDBUF,&sndbuf,len2);
if (bind(sockfd,(struct sockaddr *)(&srv_addr),sizeof(srv_addr))==-1) {
printf("can't bind! server port = %d\n",SERV_PORT); exit(1); }
printf("=====================================\n");
printf("big2 server (ver=%s) okay!\n",CLIENT_VERSION);
printf("------------------------------\nSome information:\n\npid=%d\ngetdtablesize()=%d\nfd_limit.rlim_cur=%d\n",getpid()+1,getdtablesize(),fd_limit.rlim_cur);
printf("=====================================\n");
if (fork()) exit(0);
/*
ttyfd=open("/dev/tty",O_RDWR);
ioctl(ttyfd,TIOCNOTTY,0);
close(ttyfd);
*/
umask(022);
chdir("/tmp");
signal(SIGHUP,SIG_IGN); /* SIGHUP    1    hangup */
signal(SIGINT,SIG_IGN); /* SIGINT    2    interrupt */
signal(SIGQUIT,SIG_IGN); /* SIGQUIT   3*   quit */
signal(SIGPIPE,SIG_IGN); /* SIGPIPE   13   write on a pipe or other socket with no one to read it */
/* signal(SIGCHLD,reaper); */
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* Server (pid=%d) rebooted at %s",getpid(),ctime(&now));
fflush(error_fp);
fcntl(sockfd,F_SETFL,O_NDELAY);
listen(sockfd,20);
FD_ZERO(&rfds);
FD_SET(sockfd,&rfds);
total_online=0;
serial=0;
read_log_toggle=0;
write_log_toggle=0;
last_seed=0;
onlinemax=180;
DELAY_SECOND=4;
max_error_times=2;
time(&now);
srand((localtime(&now)->tm_sec)+(localtime(&now)->tm_min));
strcpy(announce_post[0],"(Unset)");
strcpy(announce_post[1],"(Unset)");
strcpy(announce_post[2],"(Unset)");
read_database();
jmp_flag=0;
main_server_loop(1);
jmp_flag=1;
time(&now);
ping_min=localtime(&now)->tm_min;
longjmp(jmp_environment,jmp_val);
}

read_database()
{
int i;
char nick[11],pass[11];
struct stat buf;
rewind(fp);
stat(DATABASE_FILE,&buf);
if (!buf.st_size) {register_number=0; return;}
for(i=0;;i++) {
if (feof(fp)) break;
fscanf(fp,"%s %s %s %s %s %d %d %d %d\n",database[i].nick,database[i].password,database[i].uname,database[i].domain,database[i].ip,&(database[i].money),&(database[i].win),&(database[i].lose),&(database[i].login));
}
register_number=i;
}

