#include "big2d.h"

char readbuffer[1024];

char readch(fd)
int fd;
{
int j,err,len=sizeof(err);
char c;
fd_set jfds;
if (!FD_ISSET(fd,&rfds) || fd==-1) longjmp(jmp_environment,jmp_val);
j=getdtablesize();
FD_ZERO(&jfds);
FD_SET(fd,&jfds);
timeout.tv_sec=DELAY_SECOND;
timeout.tv_usec=0;
select(j,&jfds,(fd_set *)0,(fd_set *)0,(struct timeval *)&timeout);
if (!FD_ISSET(fd,&jfds)) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** fd=%03d TimeOutError *readch()* # %s",fd,ctime(&now));
fflush(error_fp);
quit_cli(fd); 
return ;}
if (!getsockopt(fd,SOL_SOCKET,SO_ERROR, &err, &len)) {
if (err) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** SocketError *readch()* %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli(fd);
longjmp(jmp_environment,jmp_val); } }
if (read(fd,&c,1)<0) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** ReadError *readch()* %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli(fd); 
return ; }
if (read_log_toggle) {
fseek(read_fp,0,SEEK_END);
fprintf(read_fp,"%c",c);
fflush(read_fp);     }
return(c);
}

writech(fd,c)
int fd;
char c;
{
int err,len=sizeof(err);
if (active[fd].bad) return;
if (!FD_ISSET(fd,&rfds) || fd==-1) return;
if (!getsockopt(fd,SOL_SOCKET,SO_ERROR, &err, &len)) {
if (err) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** SocketError *writech()* %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli3(fd);
return; } }
if (write(fd,&c,1)<0) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d writech write blocked %s # %s",fd,sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli3(fd);
return; }
if (write_log_toggle) {
fseek(write_fp,0,SEEK_END);
fprintf(write_fp,"%c",c);
fflush(write_fp);     }
}

rcv_msg(fd,s,c)
int fd;
char *s,c;
{
int i,n,err,len=sizeof(err),j;
fd_set kfds,lfds;
if (!FD_ISSET(fd,&rfds)) longjmp(jmp_environment,jmp_val);
FD_ZERO(&kfds);
FD_SET(fd,&kfds);
for(n=0;;n++) {
j=getdtablesize();
timeout.tv_sec=DELAY_SECOND;
timeout.tv_usec=0;
bcopy((char *)&kfds,(char *)&lfds,sizeof(lfds));
select(j,&lfds,(fd_set *)0,(fd_set *)0,(struct timeval *)&timeout);
if (!FD_ISSET(fd,&lfds)) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** TimeOutError # %s",ctime(&now));
fflush(error_fp);
quit_cli2(fd); 
return; }
if (!getsockopt(fd,SOL_SOCKET,SO_ERROR, &err, &len)) {
if (err) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** SocketError: %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli(fd);
longjmp(jmp_environment,jmp_val); } }
if (n>MAX_ERROR_READ_RANGE) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** MAX Read Exceeded Error # %s",ctime(&now));
fflush(error_fp);
quit_cli2(fd); 
return; }
if (read(fd,&(readbuffer[n]),1)==-1) {
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"*** ReadError: %s # %s",sys_errlist[errno],ctime(&now));
fflush(error_fp);
quit_cli(fd);
longjmp(jmp_environment,jmp_val);
}
if (readbuffer[n]==c) break; }
readbuffer[n]='\0';
strcpy(s,readbuffer);
if (read_log_toggle) {
fseek(read_fp,0,SEEK_END);
fprintf(read_fp,"%s",s);
fflush(read_fp);     }
}

new_write(fd,string,size)
int fd;
char *string;
size_t size;
{
int k;
if (active[fd].bad) return;
for(k=0;k<size;k++) writech(fd,string[k]);
if (write_log_toggle) {
fseek(write_fp,0,SEEK_END);
fprintf(write_fp,"%s",string);
fflush(write_fp); }
}

