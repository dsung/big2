#include "big2d.h"


void
cli_service(fd)
int fd;
{
if (onlineseek(fd)==-1) { /* those who have not really logined yet */
switch(readch(fd)) {
case 2:
if (active[fd].flag) {unregister_quit_cli(fd);break;}
test_register(fd);
break;

case 'L':
if (!active[fd].flag) {unregister_quit_cli(fd);break;}
check_loginname(fd);
break;

case 'P':
if (!active[fd].flag) {unregister_quit_cli(fd);break;}
check_password(fd);
break;

case 'Q':
kill_cli(fd,5);
break;

case 'd':
kill_cli(fd,4);
break;

default:
unregister_quit_cli(fd);
}
} 
else 
{
switch(readch(fd)) {
case 'A': /* ADMIN used only */
switch(readch(fd)) {
case 'b':
send_all(fd);
break;

case 'd':
shutdown_server(fd);
break;

case 'e':
exit_admin(fd);
break;

case 'm':
money_change(fd);
break;

case 'n':
max_online(fd);
break;

case 'k':
kill_user(fd);
break;

case 'l':
delay_time_change(fd);
break;

case 'M':
error_max_change(fd);
break;

case 'p':
announce(fd);
break;

case 'r':
if (read_log_toggle) {sprintf(users,"ArN"); new_write(fd,users,strlen(users)); read_log_toggle=0; return;}
sprintf(users,"ArY"); new_write(fd,users,strlen(users)); read_log_toggle=1;
break;

case 'w':
if (write_log_toggle) {sprintf(users,"AwN"); new_write(fd,users,strlen(users)); write_log_toggle=0; return;}
sprintf(users,"AwY"); new_write(fd,users,strlen(users)); write_log_toggle=1;
break;

default:
quit_cli2(fd);
}
break;

case 'B': /* BASIC USER-COMMAND */
switch(readch(fd)) {
case 'a':
admin(fd);
break;

case 'b': /* action */
action(fd);
break;

case 'd':
away_reply(fd);
break;

case 'e': /* finger reply */
finger_reply(fd);
break;

case 'f':
finger(fd);
break;

case 'g':
online[onlineseek(fd)].timeout=0;
break;

case 'i':
invite(fd);
break;

case 'j': /* join_desk */
join_desk(fd);
break;

case 'k':
kill_user2(fd);
break;

case 'l':
desk_list(fd);
break;

case 'm':
send_msg(fd);
break;

case 'n':
change_passwd(fd);
break;

case 'o':
oper(fd);
break;

case 'p':
part_desk(fd);
break;

case 'r':
rank_list(fd);
break;

case 's': /* say_words */
say_words(fd);
break;

case 't': /* topic set */
set_topic(fd);
break;

case 'u':
user_list(fd);
break;

case 'w':
who_list(fd);
break;

default:
quit_cli2(fd);
}
break;

case 'G': /* GAME Information */
switch(readch(fd)) {
case 'b': /* bet change */
change_bet(fd);
break;

case 'i': 
game_init(fd);
break;

case 'g':
get_cards(fd);
break;

default:
quit_cli2(fd);
}
break;

case 'q': /* any client wanna quit! */
quit_cli(fd);
break;

default:
quit_cli2(fd);
} /* switch */
} /* else */
}
