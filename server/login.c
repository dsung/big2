#include "big2d.h"

void
test_register(fd)
int fd;
{
if (readch(fd)!=1) {unregister_quit_cli(fd); return;}
{
char name[80],ip[80],ver[80],ip2[10];
rcv_msg(fd,name,'\n');
rcv_msg(fd,ip,'\n');
rcv_msg(fd,ver,'\n');
ip2[0]=ip[0]; ip2[1]=ip[1]; ip2[2]=ip[2]; ip2[3]='\0';
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d %s@%s [%s] registering # %s",fd,name,ip,ver,ctime(&now));
fflush(error_fp);
server_info(fd);
if (strcmp(ver,CLIENT_VERSION)) {writech(fd,1); kill_cli(fd,1); return;}
if (total_online>=onlinemax && strcmp(SYSOP_NICK,name)) {writech(fd,3); kill_cli(fd,3); return;}
writech(fd,11);
active[fd].flag=1; /* means registered */
}
}

create_new_player(nick,passwd,a,b,c)
char *nick,*passwd,*a,*b,*c;
{
int i = register_number;
strcpy(database[i].nick,nick);
strcpy(database[i].password,passwd);
strcpy(database[i].uname,a);
strcpy(database[i].domain,b);
strcpy(database[i].ip,c);
database[i].money=1000;
database[i].win=0;
database[i].lose=0;
database[i].login=1;
fseek(fp,0,SEEK_END);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",nick,passwd,a,b,c,1000,0,0,1);
fflush(fp);
++register_number;
}

check_loginname(fd)
int fd;
{
int i;
char tmp[15];
rcv_msg(fd,tmp,'\n');
if (onlineseeknick(tmp)!=-1) {
i=databaseseeknick(tmp);
sprintf(tmp,"%c%d\n",1,i);
new_write(fd,tmp,strlen(tmp)); 
return;}
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d [%10s]                   login # %s",fd,tmp,ctime(&now));
fflush(error_fp);
if ((i=databaseseeknick(tmp))==-1) writech(fd,2); else {
sprintf(tmp,"%c%d\n",3,i);
new_write(fd,tmp,strlen(tmp)); }
}

check_password(fd)
int fd;
{
int i,j;
char c,tmp[15],tmp2[15],uname[40],domain[40],ip[40],ser[10];
char Info[200];
read(fd,&c,1);
rcv_msg(fd,tmp,'\n');  /* nick */
rcv_msg(fd,tmp2,'\n'); /* passwd */
rcv_msg(fd,uname,'\n'); 
rcv_msg(fd,domain,'\n'); 
rcv_msg(fd,ip,'\n'); 
rcv_msg(fd,ser,'\n'); 
i=atoi(ser);         /* serial */
if (c=='1') {if (databaseseeknick(tmp)!=-1) {writech(fd,1); return;} create_new_player(tmp,tmp2,uname,domain,ip); i=register_number-1;}
else 
if (strcmp(database[i].password,tmp2)) {writech(fd,1); return;} 
else { /* add the login time for the old */
++database[i].login;
strcpy(database[i].uname,uname);
strcpy(database[i].domain,domain);
strcpy(database[i].ip,ip);
fseek(fp,(long)i*174,SEEK_SET);
fprintf(fp,"%10s %10s %40s %40s %40s %010d %05d %05d %05d\n",database[i].nick,database[i].password,database[i].uname,database[i].domain,database[i].ip,database[i].money,database[i].win,database[i].lose,database[i].login);
fflush(fp);
}
while ((j=onlineseeknick(tmp))!=-1) quit_cli(j);
strcpy(online[total_online].nick,tmp);
strcpy(online[total_online].password,tmp2);
strcpy(online[total_online].uname,uname);
strcpy(online[total_online].domain,domain);
strcpy(online[total_online].ip,ip);
online[total_online].fd=fd;
online[total_online].desk = -1;
online[total_online].on_game=0;
online[total_online].level=0;
online[total_online].money=database[i].money;
online[total_online].win=database[i].win;
online[total_online].lose=database[i].lose;
online[total_online].login=database[i].login;
online[total_online].serial=i;
online[total_online].error=0;
online[total_online].cardleft = 0;
online[total_online].timeout = 0;
total_online++;
serial++;
time(&now);
sprintf(Info,"%c%s%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n",2,ctime(&now),serial,total_online,register_number,i,database[i].money,database[i].win,database[i].lose,database[i].login);
new_write(fd,Info,strlen(Info));
time(&now);
fseek(error_fp,0,SEEK_END);
fprintf(error_fp,"* fd=%03d [%10s]                  passwd # %s",fd,tmp,ctime(&now));
fflush(error_fp);
}

